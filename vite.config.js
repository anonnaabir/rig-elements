import { defineConfig } from 'vite'
import path from 'path';
import { splitVendorChunkPlugin } from 'vite';
import vue from '@vitejs/plugin-vue2';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    minify: true,
    manifest: false,
    rollupOptions: {
      input: {
      'app': path.resolve(__dirname, 'src/js/app.js'),
      'ajax': path.resolve(__dirname, 'src/js/ajax.js'),
      'product-filter': path.resolve(__dirname, 'src/js/product-filter.js'),
      'lottie': path.resolve(__dirname, 'src/js/lottie.js'),
      'carousel': path.resolve(__dirname, 'src/js/carousel.js'),
      'main': path.resolve(__dirname, 'src/css/app.css'),
      'admin': path.resolve(__dirname, 'src/admin/js/admin.js'),
      'license': path.resolve(__dirname, 'src/admin/js/license.js'),
      },

        output:{
          dir: 'assets',
          watch: true,
          entryFileNames: 'js/[name].js',
          assetFileNames: '[ext]/[name].[ext]',
          manualChunks: false,
        },

    }
  }
})
