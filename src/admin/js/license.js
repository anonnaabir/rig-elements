import Vue from 'vue';
import License from './License.vue';
import LicenseDetails from './LicenseDetails.vue';

new Vue({
    el: '#license-manager',
    render: h => h(License),
});

new Vue({
    el: '#license-details',
    render: h => h(LicenseDetails),
});

// jQuery.post(
//     activator_controls.ajaxurl, {
//         'action': 'license_manager',
//         'activation': 'activated',
//     },
//     function(data, status){
//         // console.log(data);
//   });