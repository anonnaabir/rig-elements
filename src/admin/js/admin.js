import Vue from 'vue';
import App from './App.vue';
import Leads from './Leads.vue';

new Vue({
    el: '#app',
    render: h => h(App),
});

new Vue({
    el: '#leads',
    render: h => h(Leads),
});



// var template_library = new Vue({
//     el: '#app',
//     render: h => h(App),
// });

// var rig_leads = new Vue({
//     el: '#rig_leads',
//     render: h => h(Leads),
// });