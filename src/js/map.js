// import Map from 'ol/Map';
// import View from 'ol/View';
// import OSM from 'ol/source/OSM';
// import TileLayer from 'ol/layer/Tile';
// import {fromLonLat} from 'ol/proj';
// import 'ol/ol.css';

// var elem = document.getElementById('map');

// var latitude   = parseFloat(elem.dataset.latitude);
// var longitude   = parseFloat(elem.dataset.longitude);

// console.log('Data ' + latitude + ' ' + longitude);

// new Map({
//   layers: [
//     new TileLayer({source: new OSM()})
//   ],
//   view: new View({
//     center: fromLonLat([longitude, latitude]),
//     zoom: 13
//   }),
//   target: 'map'
// });

import L from 'leaflet';
import map from 'leaflet';

var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.5, -0.09]).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup();