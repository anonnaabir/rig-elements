import * as noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.css';

var slider = document.getElementById('rig-price-slider');
var min_price = parseInt(document.getElementById('price_range_start').value);
var max_price = parseInt(document.getElementById('price_range_end').value);

noUiSlider.create(slider, {
    start: [min_price, max_price],
    step: 1,
    connect: true,
    range: {
        'min': min_price,
        'max': max_price
    },
});

var stepSliderValueElement = document.getElementById('price_range_view');

slider.noUiSlider.on('update', function (values) {
    stepSliderValueElement.innerHTML = values.join(' - ');
});

var ele = document.getElementById("filter")
if(ele.addEventListener){
    ele.addEventListener("submit", function(e){
        var price_range_value = slider.noUiSlider.get(true);
        var price_range_start = price_range_value[0];
        var price_range_end = price_range_value[1];
        document.getElementById("price_range_start").value = price_range_start;
        document.getElementById("price_range_end").value = price_range_end
    });  //Modern browsers
}

// // Read the slider value.
// document.getElementById('filter_price').addEventListener('click', function () {
//     document.getElementById("product_price_range").value = slider.noUiSlider.get();
// }); 
