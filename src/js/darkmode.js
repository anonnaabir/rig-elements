import Darkmode from 'darkmode-js';

jQuery(window ).on( 'elementor/frontend/init', function() {
  elementorFrontend.hooks.addAction( 'frontend/element_ready/rig-dark-mode.default', function($scope, $){

    const darkmode = new Darkmode();
    document.getElementById("dark-toggle").addEventListener("click", function(){
        darkmode.toggle()
      });

      document.getElementById("light-toggle").addEventListener("click", function(){
        darkmode.toggle()
      });

  });
})

// import Darkmode from 'darkmode-js';
// import $ from "jquery";

// const options = {
//   bottom: '64px', // default: '32px'
//   right: 'unset', // default: '32px'
//   left: '32px', // default: 'unset'
//   time: '0.5s', // default: '0.3s'
//   mixColor: '#fff', // default: '#fff'
//   backgroundColor: '#fff',  // default: '#fff'
//   buttonColorDark: '#100f2c',  // default: '#100f2c'
//   buttonColorLight: '#fff', // default: '#fff'
//   saveInCookies: false, // default: true,
//   label: '', // default: ''
//   autoMatchOsTheme: true // default: true
// }
// const darkmode = new Darkmode();
// darkmode.showWidget();

// document.getElementById("dark-toggle").addEventListener("click", function(){
//     darkmode.toggle()
//   });

//   document.getElementById("light-toggle").addEventListener("click", function(){
//     darkmode.toggle()
//   });


  // document.getElementById("dark-mode").addEventListener("click", function(){
  //   darkmode.toggle()
  //   jQuery("#light-mode").css("display", "block");
  //   jQuery("#dark-mode").css("display", "none");
  //   localStorage.setItem("current_state", "dark");
  // });


  // document.getElementById("light-mode").addEventListener("click", function(){
  //   darkmode.toggle()
  //   jQuery("#light-mode").css("display", "none");
  //   jQuery("#dark-mode").css("display", "block");
  //   localStorage.setItem("current_state", "light");
  // });

  // var current_mode = localStorage.getItem("current_state");

  // if (current_mode == 'dark') {
  //   $("#light-mode").css("display", "block");
  //   $("#dark-mode").css("display", "none");
  // }

  // else if (current_mode == 'light') {
  //   $("#light-mode").css("display", "none");
  //   $("#dark-mode").css("display", "block");
  // }

  // else {
  //   $("#light-mode").css("display", "none");
  //   $("#dark-mode").css("display", "block");
  // }