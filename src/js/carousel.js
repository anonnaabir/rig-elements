import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';

jQuery(window ).on( 'elementor/frontend/init', function() {
  elementorFrontend.hooks.addAction( 'frontend/element_ready/rig-advance-product-category.default', function($scope, $){
    
    const swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '#rig-product-category-next',
        prevEl: '#rig-product-category-previous',
      },
    });

  });
})