jQuery(window ).on( 'elementor/frontend/init', function() {

    elementorFrontend.hooks.addAction( 'frontend/element_ready/rig-product-filter.default', function($scope, $){

	require('./price-slider');

	jQuery(document).ready(function() {
		jQuery(document).on('submit','[data-js-form=filter]',function(e){
		  e.preventDefault();
		  var ajaxurl = '/wp-admin/admin-ajax.php';
		  var data = jQuery(this).serialize();

		  jQuery.ajax({
			  url: ajaxurl,
			  data: data,
			  type: 'POST',
			  // dataType: 'html',
			  success: function(result) {
				  // console.log(result);
				  var ajax_data = result.substring(0, result.length - 1);
                  jQuery('[data-js-filter=target]').html(ajax_data);
			  }
		  });

		});
  	});

    });
 })