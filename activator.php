<?php

namespace RigElements;


if ( ! class_exists( 'Rig_Activator' ) ) {

    class Rig_Activator {

        private static $_instance = null;

			public static function instance() {
				if ( is_null( self::$_instance ) ) {
					self::$_instance = new self();
				}
				return self::$_instance;
			}

        
        
        public function __construct() {
            add_action('admin_enqueue_scripts', [ $this, 'load_activator_scripts' ]);
            add_action('wp_ajax_license_manager', [$this, 'check_product_activation' ]);
            add_action('wp_ajax_remove_activation', [$this, 'remove_product_activation' ]);
        }

        public function load_activator_scripts() {
            $current_screen = get_current_screen(); 
            $screen_1 = 'rig-elements_page_rig-elements-license-manager';
            $screen_2 = 'rig-elements_page_rig-elements-license-details';

            if ($screen_1 == $current_screen->base OR $screen_2 == $current_screen->base) {
                $license_details = get_option('rig_license');

                wp_enqueue_script( 'activator', plugins_url( '/activator/license.js', __FILE__ ), array('jquery'), '1.0', true );
                wp_localize_script( 'activator', 'activator_controls', array(
                    // 'root' => esc_url_raw( rest_url() ),
                    // 'nonce' => wp_create_nonce('wp_rest'),
                    'license_details' => isset($license_details['activation']) ? ucfirst($license_details['activation']) : null,
                    'license_type' => isset($license_details['activation']) ? ucfirst($license_details['license_type']) : null,
                    'total_sites' => isset($license_details['activation']) ? ucfirst($license_details['total_sites']) : null,
                    'used_sites' => isset($license_details['activation']) ? ucfirst($license_details['used_sites']) : null,
                    'license_key' => isset($license_details['activation']) ? ucfirst($license_details['license_key']) : null,
                    'ajaxurl' => admin_url( 'admin-ajax.php' ),
                ));
            }
        }


        public function check_product_activation() {
            $license_data = [
                'activation' => $_POST['activation'],
                'license_type' => $_POST['license_type'],
                'license_key' => $_POST['license_key'],
                'total_sites' => $_POST['total_sites'],
                'used_sites' => $_POST['used_sites'],
                'license_status' => $_POST['license_status'],
                'license_key' => $_POST['license_key'],
            ] ?? null;

            update_option('rig_license',$license_data);
        }

        public function remove_product_activation() {
            delete_option('rig_license');
        }


    }

}


Rig_Activator::instance();