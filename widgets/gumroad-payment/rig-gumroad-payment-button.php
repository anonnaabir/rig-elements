<?php
namespace RigElements\Widgets;

use Timber\Timber;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit();
} // Exit if accessed directly

class Rig_Gumroad_Payment_Button extends Widget_Base
{
    public function get_name()
    {
        return 'rig-gumroad-payment-button';
    }

    public function get_title()
    {
        return __('Gumroad Payment Button', 'rig-elements');
    }

    public function get_icon()
    {
        return 'rig-gumroad';
    }

    public function get_categories()
    {
        return ['rig_elements_widgets'];
    }

    public function get_style_depends()
    {
        return ['rig-app'];
    }

    public function get_script_depends()
    {
        return ['rig-gumroad'];
    }

    protected function register_controls() {
        // Content Controls
        $this->start_controls_section(
            'rig_gumroad_payment_content',
        [
            'label' => __('Gumroad Payment', 'rig-elements'),
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
        ]);

        $this->add_control(
			'gumorad_payment_product_link',
			[
				'label' => esc_html__( 'Product Link', 'rig-elements' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://app.gumroad.com/l/demo', 'rig-elements' ),
				'default' => [
					'url' => 'https://app.gumroad.com/l/demo',
					'is_external' => true,
					'nofollow' => true,
					'custom_attributes' => '',
				],
				'label_block' => true,
			]
		);

        $this->add_control(
			'gumorad_payment_button_text',
			[
				'label' => esc_html__( 'Button Text', 'rig-elements' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Buy On', 'rig-elements' ),
                'label_block' => true,
			]
		);

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        
        Timber::render('view.twig', [
            'gumorad_payment_product_link' => $settings['gumorad_payment_product_link']['url'],
            'gumorad_payment_button_text' => $settings['gumorad_payment_button_text'],
        ]);
    }
}
