<?php
        namespace RigElements\Widgets;

        use Timber\Timber;
        use Elementor\Widget_Base;
        use Elementor\Controls_Manager;

        if ( ! defined( 'ABSPATH' ) ) exit;


        class Rig_Open_Street_Map extends Widget_Base {


            public function get_name() {
                return 'rig-openstreetmap';
            }


            public function get_title() {
                return __( 'Opem Street Map', 'rig-elements' );
            }


            public function get_icon() {
                return 'fas fa-map-marked-alt';
            }


            public function get_categories() {
                return ['rig_elements_widgets'];
            }

            public function get_style_depends() {
                return ['rig-app'];
            }

            public function get_script_depends() {
                return ['rig-elements'];
            }



            protected function _register_controls() {
                $this->start_controls_section(
                    'section_content',
                    [
                        'label' => __( 'Open Street Map', 'rig-elements' ),
                    ]
                );

                $this->add_control(
                    'osm_latitude',
                [
                    'label' => __('Latitude', 'rig-elements'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'default' => 23.8106510,
                ]);

                $this->add_control(
                    'osm_longitude',
                [
                    'label' => __('Longitude', 'rig-elements'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'default' => 90.4126466,
                ]);

                $this->add_control(
                    'osm_zoom',
                    [
                        'label' => __( 'Zoom', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'range' => [
                            '' => [
                                'min' => 1,
                                'max' => 20,
                                'step' => 1,
                            ],
                        ],
                        'default' => [
                            'unit' => '',
                            'size' => 9,
                        ],
                    ]
                );

                $this->add_control(
                    'osm_map_height',
                    [
                        'label' => __( 'Height', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'range' => [
                            'px' => [
                                'min' => 50,
                                'max' => 800,
                                'step' => 1,
                            ],
                        ],
                        'default' => [
                            'unit' => 'px',
                            'size' => 300,
                        ],
                    ]
                );

                $this->end_controls_section();
            }


            protected function render() {
                $settings = $this->get_settings_for_display();

                Timber::render('view.twig', [
                    'osm_latitude' => $settings['osm_latitude'],
                    'osm_longitude' => $settings['osm_longitude']
                ]);
            }

            

}
