<?php
		namespace RigElements\Widgets;

		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;

		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Advance_Product_Tag extends Widget_Base {


			public function get_name() {
				return 'rig-advance-product-tag';
			}


			public function get_title() {
				return __( 'Advance Product Tag', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-product-category';
			}


			public function get_categories() {
				return [ 'rig_elements_widgets' ];
			}


			public function get_style_depends() {
				return [ 'rig-app'];
			}

			public function get_script_depends() {
				return [ 'rig-carousel'];
			}


			protected function _register_controls() {
                
                // Get All WooCommerce Product tag
				
				$product_tags = array();

				$args = array(
					'taxonomy' => 'product_tag',
					'orderby' => 'name',
					'order' => 'ASC',
					'hide_empty' => false
			   );
			   foreach( get_terms( $args ) as $tag ) :
					$product_tags[$tag->term_id] = $tag->name;
					// var_dump($tag);
			   endforeach;


            //    Controls Start


            $this->start_controls_section(
                'rig_advance_tag_query',
                [
                    'label' => __( 'Tag Query', 'rig-elements' ),
                    'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                ]
            );

            $this->add_control(
                'rig_advance_tag_query_type',
                [
                    'label' => __( 'Tag Query', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => 'all',
                    'options' => [
                        'all'  => __( 'Show All tag', 'rig-elements' ),
                        'selected' => __( 'Show Selected tag', 'plugin-domain' ),
                    ],
                ]
            );


            $this->start_controls_tabs(
                'rig_advance_tag_query_include'
            );

            $this->start_controls_tab(
                'product_tag_include_tab',
                [
                    'label' => __( 'Include', 'rig-elements' ),
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_tag_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->add_control(
                'product_tag_include',
                [
                    'label' => __( 'Tags', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => $product_tags,
                    'default' => ['all'],
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_tag_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->end_controls_tab();


            $this->start_controls_tab(
                'product_tag_exclude_tab',
                [
                    'label' => __( 'Exclude', 'rig-elements' ),
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_tag_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->add_control(
                'product_tag_exclude',
                [
                    'label' => __( 'Tags', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => $product_tags,
                    'default' => ['all'],
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_tag_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->end_controls_tab();

            $this->end_controls_tabs();    


            $this->end_controls_section();

				  // Content Controls
                
				  $this->start_controls_section(
                    'rig_product_ctg',
                    [
                        'label' => __( 'Layout', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );

                $this->add_control(
                    'rig_product_tag_style',
                    [
                        'label' => __( 'Tag Style', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'grid',
                        'options' => [
                            'grid'  => __( 'Grid', 'rig-elements' ),
                            'carousel'  => __( 'Carousel', 'rig-elements' ),
                        ],
                    ]
                );


                $this->add_responsive_control(
                    'rig_product_tag_grid_columns',
                    [
                        'label' => __( 'Columns', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
						'devices' => [ 'desktop', 'tablet', 'mobile' ],
						'desktop_default' => 'auto auto auto auto',
						'tablet_default'  => 'auto auto auto',
						'mobile_default'  => 'auto auto',
                        'options' => [
                            'auto' => __( '1 Columns', 'rig-elements' ),
							'auto auto' => __( '2 Columns', 'rig-elements' ),
                            'auto auto auto' => __( '3 Columns', 'rig-elements' ),
                            'auto auto auto auto' => __( '4 Columns', 'rig-elements' ),
                            'auto auto auto auto auto' => __( '5 Columns', 'rig-elements' ),
                            'auto auto auto auto auto auto'  => __( '6 Columns', 'rig-elements' ),
                        ],

						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-template-columns: {{options}};',
						],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_tag_style',
                                    'operator' => '==',
                                    'value' => 'grid',
                                ],
                            ],
                        ],
                    ]
                );

                
                $this->add_responsive_control(
                    'rig_product_tag_carousel_columns',
                    [
                        'label' => __( 'Columns', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => '25%',
                        'devices' => [ 'desktop', 'tablet', 'mobile' ],
                        'options' => [
                            '50%' => __( '2 Columns', 'rig-elements' ),
                            '33%' => __( '3 Columns', 'rig-elements' ),
                            '25%' => __( '4 Columns', 'rig-elements' ),
                            '20%' => __( '5 Columns', 'rig-elements' ),
                            '16%'  => __( '6 Columns', 'rig-elements' ),
                        ],
                        'selectors' => [
							'{{WRAPPER}} .rig-advance-product-category-image' => 'max-width: {{options}};',
						],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_tag_style',
                                    'operator' => '==',
                                    'value' => 'carousel',
                                ],
                            ],
                        ],
                    ]
                );
        


				$this->add_control(
                    'rig_navigation_enable',
                    [
                        'label' => __( 'Enable Navigation', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SWITCHER,
                        'enable_navigation' => __( 'Enable', 'rig-elements' ),
                        'disable_navigation' => __( 'Disable', 'rig-elements' ),
                        'return_value' => 'enable_navigation',
                        'default' => 'disable_avatar',
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_tag_style',
                                    'operator' => '==',
                                    'value' => 'carousel',
                                ],
                            ],
                        ],
                    ]
                );
                $this->add_control(
                    'rig_navigation_type',
                    [
                        'label' => __( 'Navigation Type', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::CHOOSE,
                        'options' => [
                            'text' => [
                                'title' => __( 'Text', 'rig-elements' ),
                                'icon' => 'fas fa-text-height',
                            ],
                            'icon' => [
                                'title' => __( 'Icon', 'rig-elements' ),
                                'icon' => 'fas fa-star',
                            ]
                        ],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'toggle' => true,
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_previous_text',
                    [
						
                        'label' => __( 'Previous Navigation', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
                        
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        
                        'default' => __( 'Previous Slide', 'rig-elements' ),
                    ]
                );
				$this->add_control(
                    'rig_product_ctg_next_text',
                    [
						
                        'label' => __( 'Next Navigation', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
                        
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        
                        'default' => __( 'Next Slide', 'rig-elements' ),
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_previous_icon',
                    [
                        'label' => __( 'Previous Icon', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::ICONS,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                                
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'default' => [
                            'value' => 'eicon-chevron-left',
                            'library' => 'solid',
                        ],
                    ]
                );
				$this->add_control(
                    'rig_product_ctg_next_icon',
                    [
                        'label' => __( 'Next Icon', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::ICONS,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                                
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'default' => [
                            'value' => 'eicon-chevron-right',
                            'library' => 'solid',
                        ],
                    ]
                );

				$this->end_controls_section();


                $this->start_controls_section(
                    'rig_product_tag_image_controls',
                    [
                        'label' => __( 'Tag Image', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );

                $this->add_control(
					'rig_product_tag_default_image',
					[
						'label' => __( 'Default Tag Image', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::MEDIA,
						'default' => [
							'url' => \Elementor\Utils::get_placeholder_image_src(),
						],
					]
				);

                $this->add_control(
                    'rig_product_tag_image_fit',
                    [
                        'label' => __( 'Image Fit', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'contain',
                        'options' => [
                            'contain'  => __( 'Contain', 'rig-elements' ),
                            'cover' => __( 'Cover', 'rig-elements' ),
                            'auto' => __( 'Auti', 'rig-elements' ),
                            'fill' => __( 'Fill', 'rig-elements' ),
                            'none' => __( 'None', 'rig-elements' ),
                        ],
                        'selectors' => [
							'{{WRAPPER}} .rig-advance-product-category-image img' => 'object-fit: {{options}};',
						],
                    ]
                );


                $this->add_control(
                    'rig_product_tag_image_width',
                    [
                        'label' => __( 'Image Width', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px', '%' ],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 500,
                                'step' => 1,
                            ],
                            '%' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default' => [
                            'unit' => '%',
                            'size' => 100,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'width: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_control(
                    'rig_product_tag_image_height',
                    [
                        'label' => __( 'Image Height', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px'],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 500,
                                'step' => 1,
                            ],
                        ],
                        'default' => [
                            'unit' => 'px',
                            'size' => 250,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'height: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
        


                $this->end_controls_section();

                /* Rig Advance Product tag Style Controls */

                // tag Box

                $this->start_controls_section(
                    'rig_products_tag_container_style',
                    [
                        'label' => __( 'Tag Container', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'rig_products_tag_container_background_color',
                    [
                        'label' => __( 'Background Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image' => 'background-color: {{VALUE}}',
                        ],
                    ]
                );

                
                $this->add_responsive_control(
					'rig_products_tag_container_column_gap',
					[
						'label' => __( 'Tag Column Gap', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'desktop_default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'tablet_default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'mobile_default' => [
							'unit' => 'px',
							'size' => 5,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-column-gap: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_responsive_control(
					'rig_products_tag_container_row_gap',
					[
						'label' => __( 'Tag Row Gap', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'desktop_default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'tablet_default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'mobile_default' => [
							'unit' => 'px',
							'size' => 5,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-row-gap: {{SIZE}}{{UNIT}};',
						],
					]
				);

                $this->add_responsive_control(
                    'rig_products_tag_container_padding',
                    [
                        'label' => __( 'Padding', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px','%','em'],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_control(
					'rig_products_tag_container_border_seperator',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);


                $this->start_controls_tabs(
					'rig_products_tag_container_border_controls'
				);


				// Normal Controls

				$this->start_controls_tab(
					'products_tag_wrapper_border_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_tag_wrapper_border',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper',
                    ]
                );

                			
				$this->add_responsive_control(
					'products_tag_wrapper_border_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-product-category-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_tag_wrapper_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper',
                    ]
                );

				$this->end_controls_tab();


				$this->start_controls_tab(
					'products_tag_wrapper_border_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_tag_wrapper_border_hover',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper:hover',
                    ]
                );



				$this->add_responsive_control(
					'products_tag_wrapper_border_hover_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-product-category-wrapper:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_tag_wrapper_hover_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper:hover',
                    ]
                );

				$this->end_controls_tab();

				$this->end_controls_tabs();



                $this->end_controls_section();
                
                
                // tag Name Style
                
               
                $this->start_controls_section(
                    'rig_products_tag_name_style',
                    [
                        'label' => __( 'Tag Name', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_products_tag_name_typography',
                        'label' => __( 'Tag Typography', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-image',
                    ]
                );

                $this->start_controls_tabs(
					'rig_products_tag_name_color_controls'
				);
                
                $this->start_controls_tab(
					'rig_products_tag_name_color_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);
                $this->add_control(
                    'rig_products_tag_name_color',
                    [
                        'label' => __( 'Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image p' => 'color: {{VALUE}}',
                        ],
                    ]
                );

                $this->end_controls_tab();

                $this->start_controls_tab(
					'rig_products_tag_name_color_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);
               
                $this->add_control(
                    'rig_products_tag_name_color_hover',
                    [
                        'label' => __( 'Color', 'plugin-domain' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image p:hover' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->end_controls_tab();
                $this->end_controls_tabs();
                
                $this->end_controls_section();

                
                // tag Image Styles


                $this->start_controls_section(
                    'rig_products_tag_image_styles',
                    [
                        'label' => __( 'Tag Image', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'rig_products_tag_image_background_color',
                    [
                        'label' => __( 'Background Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'background-color: {{VALUE}}',
                        ],
                    ]
                );

                
                $this->start_controls_tabs(
					'rig_products_tag_image_border_controls'
				);

                $this->start_controls_tab(
					'products_tag_image_border_normal_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);

                $this->add_control(
                    'rig-products_tag_image_padding',
                    [
                        'label' => __( 'Padding', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_tag_image_border',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-image img',
                    ]
                );

                $this->add_control(
                    'products_tag_image_border_radius',
                    [
                        'label' => __( 'Border Radius', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}}  .rig-advance-product-category-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_tag_image_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-image img',
                    ]
                );
        

                $this->end_controls_tab();

                $this->start_controls_tab(
					'products_tag_image_border_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);

                $this->end_controls_tab();
                $this->end_controls_tabs();

                
                $this->end_controls_section();
                $this->start_controls_section(
                    'rig_product_ctg_navigation_text_style',
                    [
                        'label' => __( 'Navigation Text', 'plugin-name' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],

                            ],
                        ],
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_product_ctg_navigation_text_typography',
                        'label' => __( 'Typography', 'plugin-domain' ),
                        'selector' => '{{WRAPPER}} .navigation-btn',
                    ]
                );
                $this->end_controls_section();

                $this->start_controls_section(
                    'rig_product_ctg_navigation_icon_style',
                    [
                        'label' => __( 'Navigation Icon', 'plugin-name' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                            ],
                        ],
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_navigation_icon_color',
                    [
                        'label' => __( 'Icon Color', 'plugin-domain' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig_product_ctg_icon_size' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_navigation_icon_size',
                    [
                        'label' => __( 'Width', 'plugin-domain' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px','' ],
                        'range' => [
                            'px' => [
                                'min' => 10,
                                'max' => 50,
                                'step' => 1,
                            ],
     
                        ],
                        // 'default' => [
                        //     'unit' => 'px',
                        //     'size' => 28,
                        // ],
                        'selectors' => [
                            '{{WRAPPER}} .rig_product_ctg_icon_size' => 'font-size: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
                $this->end_controls_section();
			}


			        protected function render() {
						$settings = $this->get_settings_for_display();
                        $tag_style = $settings['rig_product_tag_style'];
						$tag_default_image = $settings['rig_product_tag_default_image']['url'];
                        $tag_image_fit = $settings['rig_product_tag_image_fit'];
                        $tag_include = $settings['product_tag_include'];
                        $tag_exclude = $settings['product_tag_exclude'];
					
						$product_tags = array();

						$args = array(
							'taxonomy' => 'product_tag',
							'orderby' => 'name',
							'order' => 'ASC',
                            'include' => $tag_include,
                            'exclude' => $tag_exclude,
							'hide_empty' => false
						   );

                        $previous_navigation = $this->rig_product_ctg_previous_navigation();
                        $next_navigation = $this-> rig_product_ctg_next_navigation();
                        echo $previous_navigation;  
                        echo $next_navigation;

                        if ($tag_style == 'carousel') {
                           $tag_container_start = '<div class="rig-container-flex">
							        <div class="swiper-container swiper-full-container">
							    <div class="swiper-wrapper swiper-padding">';
                            
                           $tag_container_end = '</div></div></div>';

                           }

                        else {
                           $tag_container_start = '<div class="rig-container">';
                           $tag_container_end = '</div>';
                         }

                               echo $tag_container_start;
								
							   foreach(get_terms($args) as $tag ) :
									$product_tags[$tag->slug] = $tag->name;
									$cat_thumb_id = get_woocommerce_term_meta( $tag->term_id, 'thumbnail_id', true );
        							$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catelog');
									$term_link = get_term_link( $tag, 'product_tag' );
									// var_dump($shop_catalog_img);
									$tag_image = $shop_catalog_img[0];

									if (!isset($shop_catalog_img[0])) {
										$tag_image = $tag_default_image;
									}

                                    if ($tag_style == 'grid') {
                                        $tag_wrapper_start = '<div class="rig-product-category-wrapper">';
                                        $tag_wrapper_end = '</div>';
                                    }
     
                                    else {
                                     $tag_wrapper_start = '';
                                     $tag_wrapper_end = '';
                                    }
                                    echo $tag_wrapper_start;

									?>
									<div class="rig-advance-product-category-image swiper-slide">
									<a class="rig-woo-products-link" href='<?php echo $term_link;?>'>
									<img src="<?php echo $tag_image; ?>">
                    				<p><?php echo $tag->name;?></p>
									</a>
									</div>
									<?php
                                    echo $tag_wrapper_end;
							   endforeach;
                               
                               echo $tag_container_end;
                               
                               $next_navigation = $this-> rig_product_ctg_next_navigation();

				}

				protected function rig_product_ctg_previous_navigation(){
					$settings = $this->get_settings_for_display();
                    $previous_text = $settings['rig_product_ctg_previous_text'];
					$navigation_type = $settings['rig_navigation_type'];
					$navigation_enable = $settings['rig_navigation_enable'];
					$navigation_icon = $settings['rig_product_ctg_previous_icon'];

					    if($navigation_type == 'text'){
						    $left_navigation = '<a class="navigation-btn"  href="#" id="rig-product-category-previous" >'.$previous_text.'</a>';
					    }
					    else{
						    $left_navigation = '<a href="#" id="rig-product-category-previous" ><i class="'.$navigation_icon['value'].' rig_product_ctg_icon_size"></i></a>';
					    }
					    if($navigation_enable == 'enable_navigation'){
						    return $left_navigation;
					    }					
				}

                protected function rig_product_ctg_next_navigation(){
					$settings = $this->get_settings_for_display();
                    $next_text = $settings['rig_product_ctg_next_text'];
					$navigation_type = $settings['rig_navigation_type'];
					$navigation_enable = $settings['rig_navigation_enable'];
					$navigation_icon = $settings['rig_product_ctg_next_icon'];

					    if($navigation_type == 'text'){
						    $right_navigation = '<a class="navigation-btn" href="#" id="rig-product-category-next" >'.$next_text.'</a>';
					    }
					    else{
						    $right_navigation = '<a href="#" id="rig-product-category-next" ><i class="'.$navigation_icon['value'].' rig_product_ctg_icon_size"></i></a>';
					    }
					    if($navigation_enable == 'enable_navigation'){
						    return $right_navigation;
					    }					
				}
			
			
			
		}

		
