<?php
		namespace RigElements\Widgets;

		use Timber\Timber;
		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;
		use RigElements\Rig_Ajax_Control;

		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Product_Filter extends Widget_Base {


			public function get_name() {
				return 'rig-product-filter';
			}


			public function get_title() {
				return __( 'Product Filter', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-product-filter';
			}


			public function get_categories() {
				return [ 'rig_elements_widgets' ];
			}


			public function get_style_depends() {
				return [ 'rig-app'];
			}

			public function get_script_depends() {
				return [ 'rig-product-filter' ];
			}


			protected function _register_controls() {

				// Filter By Search

				$this->start_controls_section(
					'rig_product_filter_by_kw',
					[
						'label' => __( 'Keyword Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);

				$this->add_control(
					'keyword_filter_title',
					[
						'label' => esc_html__( 'Title', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Keyword', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);

				$this->add_control(
					'keyword_filter_placeholder',
					[
						'label' => esc_html__( 'Placeholder', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'cloth', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);


				$this->end_controls_section();


				// Filter By Price Range

				$this->start_controls_section(
					'rig_product_filter_by_price',
					[
						'label' => __( 'Price Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);


				$this->add_control(
					'price_filter_title',
					[
						'label' => esc_html__( 'Title', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Price Range', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);


				$this->add_control(
					'price_filter_range_start',
					[
						'label' => esc_html__( 'Price Range Start', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::NUMBER,
						'step' => 1,
						'default' => 0,
					]
				);


				$this->add_control(
					'price_filter_range_end',
					[
						'label' => esc_html__( 'Price Range End', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::NUMBER,
						'step' => 1,
						'default' => 500,
					]
				);


				$this->end_controls_section();


				// Filter By Category

				$this->start_controls_section(
					'rig_product_filter_by_category',
					[
						'label' => __( 'Category Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);


				$this->add_control(
					'category_filter_title',
					[
						'label' => esc_html__( 'Title', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Category', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);

				$this->end_controls_section();


				// Filter By Sales

				$this->start_controls_section(
					'rig_product_filter_by_sales',
					[
						'label' => __( 'Sales Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);


				$this->add_control(
					'sales_filter_title',
					[
						'label' => esc_html__( 'Title', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Short By', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);

				$this->end_controls_section();


				// Filter Button

				$this->start_controls_section(
					'rig_product_filter_button',
					[
						'label' => __( 'Filter Button', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
					]
				);


				$this->add_control(
					'filter_button',
					[
						'label' => esc_html__( 'Button Text', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
						'default' => esc_html__( 'Apply Filter', 'rig-elements' ),
						'placeholder' => esc_html__( 'Type your title here', 'rig-elements' ),
					]
				);

				$this->end_controls_section();



				// Product Filter Style Controls

				$this->start_controls_section(
					'rig_product_filter_container_style',
					[
						'label' => __( 'Container', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);


				$this->add_control(
					'product_filter_container_background_color',
					[
						'label' => esc_html__( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-product-filter' => 'background-color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'product_filter_container_padding',
					[
						'label' => esc_html__( 'Padding', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-product-filter' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);
		

				$this->end_controls_section();


				// Search Box Style

				$this->start_controls_section(
					'rig_product_filter_search_box_style',
					[
						'label' => __( 'Keyword Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Title Typography', 'rig-elements' ),
						'name' => 'search_box_title_typography',
						'selector' => '{{WRAPPER}} .rig-pf-search label',
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Search Box Typography', 'rig-elements' ),
						'name' => 'search_box_typography',
						'selector' => '{{WRAPPER}} .rig-pf-search',
					]
				);

				$this->add_control(
					'search_box_title_color',
					[
						'label' => esc_html__( 'Title Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search label' => 'color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'search_box_color',
					[
						'label' => esc_html__( 'Search Box Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search input' => 'color: {{VALUE}}',
						],
					]
				);


				$this->add_control(
					'search_box_background_color',
					[
						'label' => esc_html__( 'Search Box Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search input' => 'background-color: {{VALUE}}',
						],
					]
				);


				$this->add_control(
					'search_box_margin',
					[
						'label' => esc_html__( 'Margin', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search input' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'search_box_padding',
					[
						'label' => esc_html__( 'Padding', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search input' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'search_box_border',
						'label' => esc_html__( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-search input',
					]
				);

				$this->add_control(
					'search_box_border_radius',
					[
						'label' => esc_html__( 'Border Radius', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-search input' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Box_Shadow::get_type(),
					[
						'name' => 'search_box_box_shadow',
						'label' => esc_html__( 'Box Shadow', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-search input',
					]
				);

				

				$this->end_controls_section();


				$this->start_controls_section(
					'rig_product_price_filter_style',
					[
						'label' => __( 'Price Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Title Typography', 'rig-elements' ),
						'name' => 'price_filter_title_typography',
						'selector' => '{{WRAPPER}} .rig-pf-slider-title',
					]
				);

				// $this->add_group_control(
				// 	\Elementor\Group_Control_Typography::get_type(),
				// 	[
				// 		'label' => esc_html__( 'Search Box Typography', 'rig-elements' ),
				// 		'name' => 'search_box_typography',
				// 		'selector' => '{{WRAPPER}} .rig-pf-search',
				// 	]
				// );

				$this->add_control(
					'price_filter_title_color',
					[
						'label' => esc_html__( 'Title Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-slider-title' => 'color: {{VALUE}}',
						],
					]
				);


				$this->add_control(
					'price_filter_slider_divider',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);


				$this->add_control(
					'price_filter_slider_width',
					[
						'label' => esc_html__( 'Price Slider Width', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'size_units' => [ '%' ],
						'range' => [
							'%' => [
								'min' => 0,
								'max' => 100,
							],
						],
						'default' => [
							'unit' => '%',
							'size' => 100,
						],
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider' => 'width: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'price_filter_slider_height',
					[
						'label' => esc_html__( 'Price Slider Height', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider' => 'height: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'price_filter_slider_color',
					[
						'label' => esc_html__( 'Slider Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider .noUi-connect' => 'background-color: {{VALUE}}',
						],
					]
				);


				$this->add_control(
					'price_filter_handler_divider',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);


				$this->add_control(
					'price_filter_handler_height',
					[
						'label' => esc_html__( 'Handler Height', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider .noUi-handle' => 'height: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'price_filter_handler_width',
					[
						'label' => esc_html__( 'Handler Width', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider .noUi-handle' => 'width: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'price_filter_handler_border_radius',
					[
						'label' => esc_html__( 'Handler Border Radius', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider .noUi-handle' => 'border-radius: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'price_filter_handler_color',
					[
						'label' => esc_html__( 'Handler Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} #rig-price-slider .noUi-handle' => 'background-color: {{VALUE}}',
						],
					]
				);
		

				$this->end_controls_section();


				$this->start_controls_section(
					'rig_product_category_filter_style',
					[
						'label' => __( 'Category Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Title Typography', 'rig-elements' ),
						'name' => 'category_filter_title_typography',
						'selector' => '{{WRAPPER}} .rig-pf-category label',
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Cateogry Select Typography', 'rig-elements' ),
						'name' => 'category_filter_select_typography',
						'selector' => '{{WRAPPER}} .rig-pf-category select',
					]
				);

				$this->add_control(
					'category_filter_select_text_color',
					[
						'label' => esc_html__( 'Cateogry Select Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-category select' => 'color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'category_filter_select_background_color',
					[
						'label' => esc_html__( 'Cateogry Select Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-category select' => 'background-color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'category_filter_margin',
					[
						'label' => esc_html__( 'Margin', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-category select' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'category_filter_padding',
					[
						'label' => esc_html__( 'Padding', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-category select' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'category_filter_border',
						'label' => esc_html__( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-category select',
					]
				);


				$this->add_control(
					'category_filter_border_radius',
					[
						'label' => esc_html__( 'Border Radius', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-category select' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->end_controls_section();


				$this->start_controls_section(
					'rig_product_sales_filter_style',
					[
						'label' => __( 'Sales Filter', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);


				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Title Typography', 'rig-elements' ),
						'name' => 'sales_filter_title_typography',
						'selector' => '{{WRAPPER}} .rig-pf-short label',
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'label' => esc_html__( 'Cateogry Select Typography', 'rig-elements' ),
						'name' => 'sales_filter_select_typography',
						'selector' => '{{WRAPPER}} .rig-pf-short select',
					]
				);

				$this->add_control(
					'sales_filter_select_text_color',
					[
						'label' => esc_html__( 'Sales Select Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-short select' => 'color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'sales_filter_select_background_color',
					[
						'label' => esc_html__( 'Sales Select Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-short select' => 'background-color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'sales_filter_margin',
					[
						'label' => esc_html__( 'Margin', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-short select' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->add_control(
					'sales_filter_padding',
					[
						'label' => esc_html__( 'Padding', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-short select' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'sales_filter_border',
						'label' => esc_html__( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-short select',
					]
				);


				$this->add_control(
					'sales_filter_border_radius',
					[
						'label' => esc_html__( 'Border Radius', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-short select' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);
				
				
				$this->end_controls_section();


				$this->start_controls_section(
					'rig_product_filter_button_style',
					[
						'label' => __( 'Filter Button', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'name' => 'filter_button_margin',
						'selector' => '{{WRAPPER}} .rig-pf-button',
					]
				);

				$this->add_control(
					'filter_button_color',
					[
						'label' => esc_html__( 'Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-button' => 'color: {{VALUE}}',
						],
					]
				);


				$this->add_control(
					'filter_button_background_color',
					[
						'label' => esc_html__( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-pf-button' => 'background-color: {{VALUE}}',
						],
					]
				);

				$this->add_control(
					'filter_button_margin',
					[
						'label' => esc_html__( 'Margin', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);

				$this->add_control(
					'filter_button_padding',
					[
						'label' => esc_html__( 'Padding', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'filter_button_border',
						'label' => esc_html__( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-button',
					]
				);


				$this->add_control(
					'filter_button_border_radius',
					[
						'label' => esc_html__( 'Border Radius', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-pf-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);

				$this->add_group_control(
					\Elementor\Group_Control_Box_Shadow::get_type(),
					[
						'name' => 'filter_button_box_shadow',
						'label' => esc_html__( 'Box Shadow', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-pf-button',
					]
				);


				$this->end_controls_section();


			}


			protected function render() {
				$settings = $this->get_settings_for_display();

				$args = array(
					'taxonomy' => 'product_cat',
					'orderby' => 'name',
					'order' => 'ASC',
					'hide_empty' => false
				);

				foreach( get_categories( $args ) as $category ) :
					$category_list .= '<option value="'.$category->slug.'">'.$category->name.'</option>';
					endforeach;

				
				$key = [
					'total_sales' => 'Bestseller / Top Seller Products',
					'_sale_price' => 'Sale Products',
					'_wc_rating_count' => 'Highest / Top Rated Products',
					'_wc_review_count' => 'Highest / Top Reviewd Products',
					];

				foreach($key as $option => $name) :
					$meta_key .= '<option value="'.$option.'">'.$name.'</option>';
				endforeach;



				Timber::render('view.twig', [
					'keyword_filter_title' => $settings['keyword_filter_title'],
					'keyword_filter_placeholder' => $settings['keyword_filter_placeholder'],
					'price_filter_title' => $settings['price_filter_title'],
					'price_filter_range_start' => $settings['price_filter_range_start'],
					'price_filter_range_end' => $settings['price_filter_range_end'],
					'category_filter_title' => $settings['category_filter_title'],
					'sales_filter_title' => $settings['sales_filter_title'],
					'filter_button' => $settings['filter_button'],
					'category_list' => $category_list,
					'meta_key' => $meta_key
				]);

			}


		}
