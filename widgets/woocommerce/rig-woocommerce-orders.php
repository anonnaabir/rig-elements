<?php
        namespace RigElements\Widgets;

        use Elementor\Widget_Base;
        use Elementor\Controls_Manager;

        if (!defined('ABSPATH')) {
            exit();
        } // Exit if accessed directly

        class Rig_WooCommerce_Customer_Orders extends Widget_Base
        {
            public function get_name()
            {
                return 'rig-woocommerce-customer-orders';
            }

            public function get_title()
            {
                return __('WooCommerce Customer Orders', 'rig-elements');
            }

            public function get_icon()
            {
                return 'fas fa-map-signs';
            }

            public function get_categories()
            {
                return ['rig_elements_widgets'];
            }

            public function get_style_depends()
            {
                return ['core_css'];
            }

            public function get_script_depends()
            {
                return ['rig-main'];
            }

            protected function _register_controls()
            {
            }

            protected function render() {
                // global $woocommerce;
                $settings = $this->get_settings_for_display();
                $customer = wp_get_current_user();
            // Get all customer orders
                $customer_orders = get_posts(array(
                    'meta_key' => '_customer_user',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'meta_value' => get_current_user_id(),
                    'post_type' => wc_get_order_types(),
                    'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-processing'),
                ));

                foreach ($customer_orders as $customer_order) {
                    $orderq = wc_get_order($customer_order);
                    $items = $orderq->get_items();
                    // var_dump($orderq);
                    foreach ($items as $item) {
                        // var_dump($item);
                        echo $item['order_id']."<br>";
                        echo $item['name']."<br>";
                    }
                    echo $orderq->total . "<br>";
                    echo $orderq->status . "<br>";
                    echo $orderq->payment_method_title . "<br>";
                    echo "<br>";
                }

					
            }
        }
