<?php
		namespace RigElements\Widgets;

		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;

		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Advance_Product_Category extends Widget_Base {


			public function get_name() {
				return 'rig-advance-product-category';
			}


			public function get_title() {
				return __( 'Advance Product Category', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-product-category';
			}


			public function get_categories() {
				return [ 'rig_elements_widgets' ];
			}


			public function get_style_depends() {
				return [ 'rig-app'];
			}

			public function get_script_depends() {
				return [ 'rig-carousel'];
			}


			protected function _register_controls() {
                
                // Get All WooCommerce Product Category
				
				$product_cats = array();

				$args = array(
					'taxonomy' => 'product_cat',
					'orderby' => 'name',
					'order' => 'ASC',
					'hide_empty' => false
			   );
			   foreach( get_terms( $args ) as $category ) :
					$product_cats[$category->term_id] = $category->name;
					// var_dump($category);
			   endforeach;


            //    Controls Start


            $this->start_controls_section(
                'rig_advance_category_query',
                [
                    'label' => __( 'Category Query', 'rig-elements' ),
                    'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                ]
            );

            $this->add_control(
                'rig_advance_category_query_type',
                [
                    'label' => __( 'Category Query', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'default' => 'all',
                    'options' => [
                        'all'  => __( 'Show All Category', 'rig-elements' ),
                        'selected' => __( 'Show Selected Category', 'plugin-domain' ),
                    ],
                ]
            );


            $this->start_controls_tabs(
                'rig_advance_category_query_include'
            );

            $this->start_controls_tab(
                'product_category_include_tab',
                [
                    'label' => __( 'Include', 'rig-elements' ),
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_category_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->add_control(
                'product_category_include',
                [
                    'label' => __( 'Categories', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => $product_cats,
                    'default' => ['all'],
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_category_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->end_controls_tab();


            $this->start_controls_tab(
                'product_category_exclude_tab',
                [
                    'label' => __( 'Exclude', 'rig-elements' ),
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_category_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->add_control(
                'product_category_exclude',
                [
                    'label' => __( 'Categories', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => $product_cats,
                    'default' => ['all'],
                    'conditions' => [
                        'relation' => 'or',
                        'terms' => [
                            [
                                'name' => 'rig_advance_category_query_type',
                                'operator' => '==',
                                'value' => 'selected',
                            ],
                        ],
                    ],
                ]
            );

            $this->end_controls_tab();

            $this->end_controls_tabs();    


            $this->end_controls_section();

				  // Content Controls
                
				  $this->start_controls_section(
                    'rig_product_ctg',
                    [
                        'label' => __( 'Layout', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );

                $this->add_control(
                    'rig_product_category_style',
                    [
                        'label' => __( 'Category Style', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'grid',
                        'options' => [
                            'grid'  => __( 'Grid', 'rig-elements' ),
                            'carousel'  => __( 'Carousel', 'rig-elements' ),
                        ],
                    ]
                );


                $this->add_responsive_control(
                    'rig_product_category_grid_columns',
                    [
                        'label' => __( 'Columns', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
						'devices' => [ 'desktop', 'tablet', 'mobile' ],
						'desktop_default' => 'auto auto auto auto',
						'tablet_default'  => 'auto auto auto',
						'mobile_default'  => 'auto auto',
                        'options' => [
                            'auto' => __( '1 Columns', 'rig-elements' ),
							'auto auto' => __( '2 Columns', 'rig-elements' ),
                            'auto auto auto' => __( '3 Columns', 'rig-elements' ),
                            'auto auto auto auto' => __( '4 Columns', 'rig-elements' ),
                            'auto auto auto auto auto' => __( '5 Columns', 'rig-elements' ),
                            'auto auto auto auto auto auto'  => __( '6 Columns', 'rig-elements' ),
                        ],

						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-template-columns: {{options}};',
						],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_category_style',
                                    'operator' => '==',
                                    'value' => 'grid',
                                ],
                            ],
                        ],
                    ]
                );

                
                $this->add_responsive_control(
                    'rig_product_category_carousel_columns',
                    [
                        'label' => __( 'Columns', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => '25%',
                        'devices' => [ 'desktop', 'tablet', 'mobile' ],
                        'options' => [
                            '50%' => __( '2 Columns', 'rig-elements' ),
                            '33%' => __( '3 Columns', 'rig-elements' ),
                            '25%' => __( '4 Columns', 'rig-elements' ),
                            '20%' => __( '5 Columns', 'rig-elements' ),
                            '16%'  => __( '6 Columns', 'rig-elements' ),
                        ],
                        'selectors' => [
							'{{WRAPPER}} .rig-advance-product-category-image' => 'max-width: {{options}};',
						],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_category_style',
                                    'operator' => '==',
                                    'value' => 'carousel',
                                ],
                            ],
                        ],
                    ]
                );
        


				$this->add_control(
                    'rig_navigation_enable',
                    [
                        'label' => __( 'Enable Navigation', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SWITCHER,
                        'enable_navigation' => __( 'Enable', 'rig-elements' ),
                        'disable_navigation' => __( 'Disable', 'rig-elements' ),
                        'return_value' => 'enable_navigation',
                        'default' => 'disable_avatar',
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_product_category_style',
                                    'operator' => '==',
                                    'value' => 'carousel',
                                ],
                            ],
                        ],
                    ]
                );
                $this->add_control(
                    'rig_navigation_type',
                    [
                        'label' => __( 'Navigation Type', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::CHOOSE,
                        'options' => [
                            'text' => [
                                'title' => __( 'Text', 'rig-elements' ),
                                'icon' => 'fas fa-text-height',
                            ],
                            'icon' => [
                                'title' => __( 'Icon', 'rig-elements' ),
                                'icon' => 'fas fa-star',
                            ]
                        ],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'toggle' => true,
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_previous_text',
                    [
						
                        'label' => __( 'Previous Navigation', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
                        
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        
                        'default' => __( 'Previous Slide', 'rig-elements' ),
                    ]
                );
				$this->add_control(
                    'rig_product_ctg_next_text',
                    [
						
                        'label' => __( 'Next Navigation', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::TEXT,
                        
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        
                        'default' => __( 'Next Slide', 'rig-elements' ),
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_previous_icon',
                    [
                        'label' => __( 'Previous Icon', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::ICONS,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                                
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'default' => [
                            'value' => 'eicon-chevron-left',
                            'library' => 'solid',
                        ],
                    ]
                );
				$this->add_control(
                    'rig_product_ctg_next_icon',
                    [
                        'label' => __( 'Next Icon', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::ICONS,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                                
                                [
                                    'name' => 'rig_navigation_enable',
                                    'operator' => '==',
                                    'value' => 'enable_navigation',
                                ],
                            ],
                        ],
                        'default' => [
                            'value' => 'eicon-chevron-right',
                            'library' => 'solid',
                        ],
                    ]
                );

				$this->end_controls_section();


                $this->start_controls_section(
                    'rig_product_category_image_controls',
                    [
                        'label' => __( 'Category Image', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );

                $this->add_control(
					'rig_product_category_default_image',
					[
						'label' => __( 'Default Category Image', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::MEDIA,
						'default' => [
							'url' => \Elementor\Utils::get_placeholder_image_src(),
						],
					]
				);

                $this->add_control(
                    'rig_product_category_image_fit',
                    [
                        'label' => __( 'Image Fit', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'contain',
                        'options' => [
                            'contain'  => __( 'Contain', 'rig-elements' ),
                            'cover' => __( 'Cover', 'rig-elements' ),
                            'auto' => __( 'Auti', 'rig-elements' ),
                            'fill' => __( 'Fill', 'rig-elements' ),
                            'none' => __( 'None', 'rig-elements' ),
                        ],
                        'selectors' => [
							'{{WRAPPER}} .rig-advance-product-category-image img' => 'object-fit: {{options}};',
						],
                    ]
                );


                $this->add_control(
                    'rig_product_category_image_width',
                    [
                        'label' => __( 'Image Width', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px', '%' ],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 500,
                                'step' => 1,
                            ],
                            '%' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'default' => [
                            'unit' => '%',
                            'size' => 100,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'width: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_control(
                    'rig_product_category_image_height',
                    [
                        'label' => __( 'Image Height', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px'],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 500,
                                'step' => 1,
                            ],
                        ],
                        'default' => [
                            'unit' => 'px',
                            'size' => 250,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'height: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
        


                $this->end_controls_section();

                /* Rig Advance Product Category Style Controls */

                // Category Box

                $this->start_controls_section(
                    'rig_products_category_container_style',
                    [
                        'label' => __( 'Category Container', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'rig_products_category_container_background_color',
                    [
                        'label' => __( 'Background Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image' => 'background-color: {{VALUE}}',
                        ],
                    ]
                );

                
                $this->add_responsive_control(
					'rig_products_category_container_column_gap',
					[
						'label' => __( 'Category Column Gap', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'desktop_default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'tablet_default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'mobile_default' => [
							'unit' => 'px',
							'size' => 5,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-column-gap: {{SIZE}}{{UNIT}};',
						],
					]
				);


				$this->add_responsive_control(
					'rig_products_category_container_row_gap',
					[
						'label' => __( 'Category Row Gap', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px'],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'desktop_default' => [
							'unit' => 'px',
							'size' => 15,
						],
						'tablet_default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'mobile_default' => [
							'unit' => 'px',
							'size' => 5,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-container' => 'grid-row-gap: {{SIZE}}{{UNIT}};',
						],
					]
				);

                $this->add_responsive_control(
                    'rig_products_category_container_padding',
                    [
                        'label' => __( 'Padding', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px','%','em'],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_control(
					'rig_products_category_container_border_seperator',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);


                $this->start_controls_tabs(
					'rig_products_category_container_border_controls'
				);


				// Normal Controls

				$this->start_controls_tab(
					'products_category_wrapper_border_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_category_wrapper_border',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper',
                    ]
                );

                			
				$this->add_responsive_control(
					'products_category_wrapper_border_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-product-category-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_category_wrapper_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper',
                    ]
                );

				$this->end_controls_tab();


				$this->start_controls_tab(
					'products_category_wrapper_border_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_category_wrapper_border_hover',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper:hover',
                    ]
                );



				$this->add_responsive_control(
					'products_category_wrapper_border_hover_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-product-category-wrapper:hover' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);


                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_category_wrapper_hover_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-category-wrapper:hover',
                    ]
                );

				$this->end_controls_tab();

				$this->end_controls_tabs();



                $this->end_controls_section();
                
                
                // Category Name Style
                
               
                $this->start_controls_section(
                    'rig_products_category_name_style',
                    [
                        'label' => __( 'Category Name', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_products_category_name_typography',
                        'label' => __( 'Category Typography', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-name',
                    ]
                );

                $this->add_responsive_control(
					'rig_products_category_name_alignment',
					[
						'label' => __( 'Text Align', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::CHOOSE,
						'options' => [
							'left' => [
								'title' => __( 'Left', 'rig-elements' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => __( 'Center', 'rig-elements' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => __( 'Right', 'rig-elements' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'default' => 'center',
						'devices' => [ 'desktop', 'tablet' ],
						'selectors' => [
							'{{WRAPPER}} .rig-advance-product-category-name' => 'text-align: {{VALUE}};',
						],
					]
				);


                $this->add_responsive_control(
                    'rig_products_category_name_margin',
                    [
                        'label' => __( 'Margin', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-name' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_responsive_control(
                    'rig_products_category_name_padding',
                    [
                        'label' => __( 'Padding', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-name' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );

                $this->start_controls_tabs(
					'rig_products_category_name_color_controls'
				);
                
                $this->start_controls_tab(
					'rig_products_category_name_color_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);
                $this->add_control(
                    'rig_products_category_name_color',
                    [
                        'label' => __( 'Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-name' => 'color: {{VALUE}}',
                        ],
                    ]
                );

                $this->end_controls_tab();

                $this->start_controls_tab(
					'rig_products_category_name_color_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);
               
                $this->add_control(
                    'rig_products_category_name_color_hover',
                    [
                        'label' => __( 'Color', 'plugin-domain' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-name p:hover' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->end_controls_tab();
                $this->end_controls_tabs();
                
                $this->end_controls_section();

                
                // Category Image Styles


                $this->start_controls_section(
                    'rig_products_category_image_styles',
                    [
                        'label' => __( 'Category Image', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'rig_products_category_image_background_color',
                    [
                        'label' => __( 'Background Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'background-color: {{VALUE}}',
                        ],
                    ]
                );

                
                $this->start_controls_tabs(
					'rig_products_category_image_border_controls'
				);

                $this->start_controls_tab(
					'products_category_image_border_normal_tab',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);

                $this->add_responsive_control(
                    'rig-products_category_image_padding',
                    [
                        'label' => __( 'Padding', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-advance-product-category-image img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'products_category_image_border',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-image img',
                    ]
                );

                $this->add_control(
                    'products_category_image_border_radius',
                    [
                        'label' => __( 'Border Radius', 'rig-elements' ),
                        'type' => Controls_Manager::DIMENSIONS,
                        'size_units' => [ 'px', '%', 'em' ],
                        'selectors' => [
                            '{{WRAPPER}}  .rig-advance-product-category-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                        ],
                    ]
                );

                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'products_category_image_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-advance-product-category-image img',
                    ]
                );
        

                $this->end_controls_tab();

                $this->start_controls_tab(
					'products_category_image_border_hover_tab',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);

                $this->end_controls_tab();
                $this->end_controls_tabs();

                
                $this->end_controls_section();
                $this->start_controls_section(
                    'rig_product_ctg_navigation_text_style',
                    [
                        'label' => __( 'Navigation Text', 'plugin-name' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'text',
                                ],

                            ],
                        ],
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_product_ctg_navigation_text_typography',
                        'label' => __( 'Typography', 'plugin-domain' ),
                        'selector' => '{{WRAPPER}} .navigation-btn',
                    ]
                );
                $this->end_controls_section();

                $this->start_controls_section(
                    'rig_product_ctg_navigation_icon_style',
                    [
                        'label' => __( 'Navigation Icon', 'plugin-name' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                        'conditions' => [
                            'relation' => 'and',
                            'terms' => [
                                [
                                    'name' => 'rig_navigation_type',
                                    'operator' => '==',
                                    'value' => 'icon',
                                ],
                            ],
                        ],
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_navigation_icon_color',
                    [
                        'label' => __( 'Icon Color', 'plugin-domain' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rig_product_ctg_icon_size' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->add_control(
                    'rig_product_ctg_navigation_icon_size',
                    [
                        'label' => __( 'Width', 'plugin-domain' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px','' ],
                        'range' => [
                            'px' => [
                                'min' => 10,
                                'max' => 50,
                                'step' => 1,
                            ],
     
                        ],
                        // 'default' => [
                        //     'unit' => 'px',
                        //     'size' => 28,
                        // ],
                        'selectors' => [
                            '{{WRAPPER}} .rig_product_ctg_icon_size' => 'font-size: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
                $this->end_controls_section();
			}


			protected function render() {
								$settings = $this->get_settings_for_display();
                                $category_style = $settings['rig_product_category_style'];
								$category_default_image = $settings['rig_product_category_default_image']['url'];
                                $category_image_fit = $settings['rig_product_category_image_fit'];
                                $category_include = $settings['product_category_include'];
                                $category_exclude = $settings['product_category_exclude'];
					
								$product_cats = array();

								$args = array(
									'taxonomy' => 'product_cat',
									'orderby' => 'name',
									'order' => 'ASC',
                                    'include' => $category_include,
                                    'exclude' => $category_exclude,
									'hide_empty' => false
							   );

                               $previous_navigation = $this->rig_product_ctg_previous_navigation();
                               $next_navigation = $this-> rig_product_ctg_next_navigation();
                               echo $previous_navigation;  
                               echo $next_navigation;

                               if ($category_style == 'carousel') {
                                   $category_container_start = '
                                   <div class="flex">
							        <div class="swiper-container swiper-full-container" data-glide-el="track">
							    <div class="swiper-wrapper swiper-padding">';
                                $category_container_end = '
                                </div>
                                </div>
                                </div>';
                               }

                               else {
                                $category_container_start = '<div class="rig-container grid mobile:grid-cols-2 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 xl:grid-cols-4 gap-1">';
                                $category_container_end = '</div>';
                               }

                               echo $category_container_start;
								
							   foreach(get_terms($args) as $category ) :
									$product_cats[$category->slug] = $category->name;
									$cat_thumb_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
        							$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catelog');
									$term_link = get_term_link( $category, 'product_cat' );
									// var_dump($shop_catalog_img);
									$category_image = $shop_catalog_img[0];

									if (!isset($shop_catalog_img[0])) {
										$category_image = $category_default_image;
									}

                                    if ($category_style == 'grid') {
                                        $category_wrapper_start = '<div class="rig-product-category-wrapper">';
                                        $category_wrapper_end = '</div>';
                                    }
     
                                    else {
                                     $category_wrapper_start = '';
                                     $category_wrapper_end = '';
                                    }
                                    echo $category_wrapper_start;
									?>
									<div class="rig-advance-product-category-image swiper-slide">
									<a class="rig-woo-products-link" href='<?php echo $term_link;?>'>
									<img src="<?php echo $category_image; ?>">
                    				<p class="rig-advance-product-category-name"><?php echo $category->name;?></p>
									</a>
									</div>
									<?php
                                    echo $category_wrapper_end;
							   endforeach;
                               
                               echo $category_container_end;
                               
                               $next_navigation = $this-> rig_product_ctg_next_navigation();

				}

				protected function rig_product_ctg_previous_navigation(){
					$settings = $this->get_settings_for_display();
                    $previous_text = $settings['rig_product_ctg_previous_text'];
					$navigation_type = $settings['rig_navigation_type'];
					$navigation_enable = $settings['rig_navigation_enable'];
					$navigation_icon = $settings['rig_product_ctg_previous_icon'];

					    if($navigation_type == 'text'){
						    $left_navigation = '<a class="navigation-btn"  href="#" id="rig-product-category-previous" >'.$previous_text.'</a>';
					    }
					    else{
						    $left_navigation = '<a href="#" id="rig-product-category-previous" ><i class="'.$navigation_icon['value'].' rig_product_ctg_icon_size"></i></a>';
					    }
					    if($navigation_enable == 'enable_navigation'){
						    return $left_navigation;
					    }					
				}

                protected function rig_product_ctg_next_navigation(){
					$settings = $this->get_settings_for_display();
                    $next_text = $settings['rig_product_ctg_next_text'];
					$navigation_type = $settings['rig_navigation_type'];
					$navigation_enable = $settings['rig_navigation_enable'];
					$navigation_icon = $settings['rig_product_ctg_next_icon'];

					    if($navigation_type == 'text'){
						    $right_navigation = '<a class="navigation-btn" href="#" id="rig-product-category-next" >'.$next_text.'</a>';
					    }
					    else{
						    $right_navigation = '<a href="#" id="rig-product-category-next" ><i class="'.$navigation_icon['value'].' rig_product_ctg_icon_size"></i></a>';
					    }
					    if($navigation_enable == 'enable_navigation'){
						    return $right_navigation;
					    }					
				}			
			
		}

		
