<?php
    namespace RigElements\Widgets;
    
    use Elementor\Widget_Base;
    use Elementor\Controls_Manager;

    if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class Rig_Flickr_Embed extends Widget_Base {
        
        public function get_name(){
            
            return 'rig-flickr';
        }

        public function get_title() {
            return __('Flickr', 'rig-elements');
        }

        public function get_icon() {
            return 'rig-flickr';
        }

        public function get_categories() {
            return ['rig_elements_widgets'];
        }

        public function get_style_depends() {
            return ['rig-app'];
        }

        public function get_script_depends() {
            return ['rig-elements'];
        }

        protected function _register_controls() {
            // Content Controls

            $this->start_controls_section(
                'rig_flickr_embed_contols',
            [
                'label' => __('Flickr Link', 'rig-elements'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]);

            $this->add_control(
                'flickr_link',
                [
                    'label' => esc_html__( 'Image Link', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::URL,
                    'placeholder' => esc_html__( 'https://your-link.com', 'rig-elements' ),
                    'default' => [
                        'url' => 'https://www.flickr.com/photos/thebetterday4u/50802316192/in/photolist-2kpe9Fj-2mfd4oo-2mGRqGW-2mfgT7n-2mfd4vh-2m8K77r-2kp9Kom-2kp9KuZ-2kpe9yv-2kp9Kkf-2kpdyuT-2kpe9uH-2kpdyzY-2kpdyoL-2kp9Kqk-2kpe9y5-2kpe9ta-2kpdyrS-2kVMKmC-2kpe9xZ-2kp9Krs-2j7TFEN-2kp9Kmx-2kpdyA9-2kUGnok-2m1uZqZ-2meSJwo-2mexnrU-2mbkv5p-2m6ox5J-2m6xwf8-2mmkbRa-2kqG2C1-2mGHdbE-2kpe9wr-2kijThs-2kfYZVL-2khcpg7-2kf4rtg-2kiyeAH-2kULWka-2kUGnjn-2kUGniq-2keZda6-2kULWji-2kUGnhU-2kUKQ3k-2kf41QT-2kUPPat-2kUKQ66',
                        'is_external' => true,
                        'nofollow' => true,
                        'custom_attributes' => '',
                    ],
                ]
            );
    

            $this->end_controls_section();


            // Style Controls

            $this->start_controls_section(
                'rig_flickr_embed_style',
            [
                'label' => __('Embed Background', 'rig-elements'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]);

            $this->add_control(
                'flickr_padding',
                [
                    'label' => esc_html__( 'Padding', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', '%', 'em' ],
                    'selectors' => [
                        '{{WRAPPER}} .rig-flickr-embed' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_control(
                'flickr_background_color',
                [
                    'label' => esc_html__( 'Background Color', 'plugin-name' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .rig-flickr-embed' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
    
    


            $this->end_controls_section();

        }

        protected function render() {
            $settings = $this->get_settings_for_display();

            $url = $settings['flickr_link']['url'];
            // $args = array( 
            //     'width' => 612, 
            //     'height' => 344,
            //     'data-theme' => 'dark'
            // );

            $oembed = _wp_oembed_get_object();
            $oembed_provider = $oembed->get_provider( $url);
            $oembed_data = $oembed->fetch( $oembed_provider, $url);

            if ( $oembed_data ) {
                echo '<div class="rig-flickr-embed">'.$oembed_data->html.'</div>';
            }
            
        }
    }
