<?php

namespace RigElements\Widgets;

if ( ! class_exists( 'Rig_Menu' ) ) {
    class Rig_Menu extends \Walker_Nav_Menu {

        function start_lvl( &$output, $depth = 1, $arg = [] ) {
            $output .= "<ul class='hidden md:flex space-x-6 font-medium text-sm'>";

            }  

            function end_lvl(&$output, $depth=0, $args=null) { 
                $output .= "</ul>";
            }
       
        
        function start_el(&$output, $item, $depth=2, $args=[], $id=0) {
            if ($args->walker->has_children) {
                $meta = get_post_meta( $item->ID, 'rig_menu', true );
                $meta2 = get_post_meta( $item->ID, 'opt-repeater-1', true );
                $allmeta = array_column($meta2, 'opt-text');
                // var_dump($allmeta);
                // $output .= "<li class='dropdown " .  implode(" ", $item->classes) . "'>";
            // $output .= '<div class="dropdown">';
            $output .= '<li>';
            $output .= '<a class="hover:text-indigo-600 cursor-pointer" href="' . $item->url . '">';
            $output .= $item->title; 
            $output .= '</a>';
            foreach ($allmeta as $menu_items) {
                $output .= '<a>'.$menu_items.'</a>'; 
            }
            }

            else {
                $output .= '<li>';
                $output .= '<a class="hover:text-indigo-600 cursor-pointer href="' . $item->url . '">';
                $output .= $item->title;
                $output .= '</a>';
            }
        }


        function end_el(&$output, $item, $depth=0, $args=null) { 
            // if ($args->walker->has_children) {
            $output .= '</li>';
            // }
        }


    }
}
        