<?php

namespace RigElements\Widgets;

if ( ! class_exists( 'WP_Bootstrap_Navwalker' ) ) {
    class Rig_Menu_Walker extends \Walker_Nav_Menu {

        function start_lvl( &$output, $depth = 2, $arg = [] ) {  
            $output .= '<ul id="dropdown-items">';

        }  

            function end_lvl(&$output, $depth=2, $args=null) {
                    $output .= "</ul>";
            }
       
        
        function start_el(&$output, $item, $depth=2, $args=[], $id=0) {
            if ($args->walker->has_children) {
            $output .= '<li>';
            $output .= '<a id="dropdown-menu" class="flex py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white" href="' . $item->url . '">';
            $output .= $item->title; 
            $output .= '<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
          </svg>';
            $output .= '</a>';
            }

            else {
                $output .= '<li>';
                $output .= '<a class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400 dark:hover:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent" href="' . $item->url . '">';
                $output .= $item->title;
                $output .= '</a>';
            }
        }


        function end_el(&$output, $item, $depth=0, $args=null) { 
            // if ($args->walker->has_children) {
            $output .= '</li>';
            // }
        }


    }
}
        