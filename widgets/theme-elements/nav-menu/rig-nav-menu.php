<?php
		namespace RigElements\Widgets;

		use Timber\Timber;
		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;


		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Nav_Menu extends Widget_Base {


			public function get_name() {
				return 'rig-nav-menu';
			}


			public function get_title() {
				return __( 'Navigation Menu', 'rig-elements' );
			}


			public function get_icon() {
				return '';
			}


			public function get_categories() {
				return [ 'rig_elements_single_widgets' ];
			}


			public function get_style_depends() {
				return [ 'core_css'];
			}

			public function get_script_depends() {
				return [ 'rig-main' ];
			}


			protected function register_controls() {

				
				$this->end_controls_section();

			}


			protected function render() {
				$settings = $this->get_settings_for_display();

				// $preview_title = Rig_Query_Control::get_preview_title();
				// $post_title = Rig_Query_Control::get_the_title();
				// $elementor_preview_mode = Rig_Query_Control::is_elementor_preview_mode();

                // $menu = get_term( $locations[$theme_location], 'nav_menu' );
                $menu_items = wp_get_nav_menu_items('main-menu');

                foreach ($menu_items as $menu){
                    $item = '<p>'.$menu->title.'</p>';
                    $parent = $menu->ID;

                    if ($menu->menu_item_parent != 0) {
                        $item = '<h2>'.$menu->title.'</h2>';

                        // foreach ($menu_items as $submenu) {
                        //     if ($submenu->menu_item_parent == $parent) {
                        //         $item = '<h4><strong>'.$menu->title.'</strong></h4>';
                        //     }
                        // }
                    }

                    echo $item;
                    // var_dump($menu);
                }

                // var_dump($menu_items);
                // echo $menu_items;

				Timber::render('view.twig', [
					
				]);

				
			}
			
		}