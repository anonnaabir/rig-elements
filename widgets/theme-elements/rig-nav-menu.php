<?php
		namespace RigElements\Widgets;

		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;
		use RigElements\Rig_Query_Control;
		

		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Nav_Menu extends Widget_Base {


			public function get_name() {
				return 'rig-nav-menu';
			}


			public function get_title() {
				return __( 'Navigation Menu', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-Nav-Menu';
			}


			public function get_categories() {
				return [ 'rig_elements_header_footer_widgets' ];
			}


			public function get_style_depends() {
				return [ 'core_css','rig-app'];
			}

			public function get_script_depends() {
				return [ 'rig-elements' ];
			}


			protected function register_controls() {

                $nav_menus = array();

                $registerd_menus = Rig_Query_Control::get_all_nav_menus();
                foreach ( $registerd_menus as $menu) {
                    $nav_menus[$menu->term_id] = $menu->name;
                }

                $this->start_controls_section(
                    'rig_nav_menu_layout_section',
                    [
                        'label' => __( 'Layout', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );
        
                $this->add_control(
                    'rig_nav_menu_list',
                    [
                        'label' => __( 'Available Menus', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'solid',
                        'options' => $nav_menus,
                    ]
                );

				$this->add_control(
					'rig_nav_menu_layout',
					[
						'label' => __( 'Layout', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SELECT,
						'default' => 'row',
						'options' => [
							'row'  => __( 'Horizontal', 'rig-elements' ),
							'column'  => __( 'Vertical', 'rig-elements' ),
							// 'dropdown'  => __( 'Dropdown', 'rig-elements' ),
						],

						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu' => 'flex-direction: {{options}};',
						],
					]
				);

				$this->add_control(
					'rig_nav_menu_align',
					[
						'label' => __( 'Align', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::CHOOSE,
						'options' => [
							'start' => [
								'title' => __( 'Left', 'rig-elements' ),
								'icon' => 'eicon-h-align-left',
							],
							'center' => [
								'title' => __( 'Center', 'rig-elements' ),
								'icon' => 'eicon-h-align-center',
							],
							'flex-end' => [
								'title' => __( 'Right', 'rig-elements' ),
								'icon' => 'eicon-h-align-right',
							],
							'space-around' => [
								'title' => __( 'Stretch', 'rig-elements' ),
								'icon' => 'eicon-h-align-stretch',
							],
						],
						'default' => 'center',
						'toggle' => true,
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu' => 'justify-content: {{options}};',
						],
					]
				);

				// $this->add_control(
				// 	'rig_nav_menu_submenu_indicator',
				// 	[
				// 		'label' => __( 'Submenu Indicator', 'rig-elements' ),
				// 		'type' => \Elementor\Controls_Manager::SELECT,
				// 		'default' => 'classic',
				// 		'options' => [
				// 			'classic'  => __( 'Classic', 'rig-elements' ),
				// 			'chevron'  => __( 'Chevron', 'rig-elements' ),
				// 			'Angle'  => __( 'Angle', 'rig-elements' ),
				// 			'plus'  => __( 'Plus', 'rig-elements' ),
				// 			'none'  => __( 'None', 'rig-elements' ),
				// 		],
				// 	]
				// );
				
				$this->add_control(
					'rig_nav_menu_mobile_dropdown_heading',
					[
						'label' => __( 'Mobile Dropdown', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::HEADING,
						'separator' => 'before',
					]
				);
				// $this->add_control(
				// 	'rig_nav_menu_side_align',
				// 	[
				// 		'label' => __( 'Align', 'rig-elements' ),
				// 		'type' => \Elementor\Controls_Manager::SELECT,
				// 		'default' => 'aside',
				// 		'options' => [
				// 			'left'  => __( 'Aside', 'rig-elements' ),
				// 			'center' => __( 'Center', 'rig-elements' ),
				// 		],
				// 		'default' => 'left',
				// 		'toggle' => true,
				// 		'selectors' => [
				// 			' @media (max-width: 767px){ {{WRAPPER}} .elementor-256 .elementor-element.elementor-element-d8a5fc5 .rig-nav-menu a}' => 'text-align: {{VALUE}};',
				// 		],
						
						
				// 	]
				// );
				$this->add_control(
					'rig_nav_menu_toggle_button',
					[
						'label' => __( 'Toggle Button', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::SELECT,
						'default' => 'hamburger',
						'options' => [
							'fas fa-bars'  => __( 'Hamburger', 'rig-elements' ),
							'fas fa-ellipsis-v' => __( 'Dotted', 'rig-elements' ),
						],
						'default' => 'fas fa-bars',
					]
				);
				$this->add_control(
					'text_align',
					[
						'label' => __( 'Alignment', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::CHOOSE,
						'options' => [
							'start' => [
								'title' => __( 'Left', 'rig-elements' ),
								'icon' => 'eicon-h-align-left',
							],
							'center' => [
								'title' => __( 'Center', 'rig-elements' ),
								'icon' => 'eicon-h-align-center',
							],
							'end' => [
								'title' => __( 'Right', 'rig-elements' ),
								'icon' => 'eicon-h-align-right',
							],
							
						],	
						'selectors' => [
							'{{WRAPPER}} .toggle-btn-class' => 'justify-content: {{VALUE}}',
						],
						'default' => 'start',
					]
				);
                $this->end_controls_section();
				$this->start_controls_section(
					'rig_nav_menu_style_section',
					[
						'label' => __( 'Main Menu', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);
				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'name' => 'rig_nav_menu_typography',
						'label' => __( 'Typography', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-nav-menu a',
					]
				);
				$this->start_controls_tabs('style_tabs');
				$this->start_controls_tab(
					'rig_nav_menu_normal',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);

				$this->add_control(
					'rig_nav_menu_normal_color',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu a' => 'color: {{VALUE}}',
						],
					]
				);

				// $this->add_control(
				// 	'rig_nav_menu_background_color',
				// 	[
				// 		'label' => __( 'Text Color', 'rig-elements' ),
				// 		'type' => \Elementor\Controls_Manager::COLOR,
				// 		'selectors' => [
				// 			'{{WRAPPER}} .rig-nav-menu' => 'background-color: {{VALUE}}',
				// 		],
				// 	]
				// );

				$this->end_controls_tab();
				$this->start_controls_tab(
					'rig_nav_menu_hover',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_hover_color',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu a:hover' => 'color: {{VALUE}}',
						],
					]
				);
				$this->end_controls_tab();
				$this->start_controls_tab(
					'rig_nav_menu_active',
					[
						'label' => __( 'Active', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_active_color',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu a:active' => 'color: {{VALUE}}',
						],
					]
				);
				$this->end_controls_tab();
				$this->end_controls_tabs();
				$this->add_control(
					'rig_nav_menu_main_menu_section_divider',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);

				$this->add_responsive_control(
					'rig_nav_menu_horizontal_padding',
					[
						'label' => __( 'Horizontal Padding', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}}  .rig-nav-menu li' => 'padding-top: {{SIZE}}{{UNIT}};padding-bottom: {{SIZE}}{{UNIT}}',
						],
					]
				);

				$this->add_responsive_control(
					'rig_nav_menu_vertical_padding',
					[
						'label' => __( 'Vertical Padding', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px',],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 5,
							],

						],
						'default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li' => 'padding-right: {{SIZE}}{{UNIT}};padding-left: {{SIZE}}{{UNIT}};',
						],
					]
				);
				// $this->add_responsive_control(
				// 	'rig_nav_menu_space_between',
				// 	[
				// 		'label' => __( 'Space Between', 'rig-elements' ),
				// 		'type' => Controls_Manager::SLIDER,
				// 		'size_units' => [ 'px' ],
				// 		'range' => [
				// 			'px' => [
				// 				'min' => 0,
				// 				'max' => 50,
				// 				'step' => 1,
				// 			],
				// 		],
				// 		'default' => [
				// 			'unit' => 'px',
				// 			'size' => 20,
				// 		],
				// 		'selectors' => [
				// 			'{{WRAPPER}} .rig-nav-menu li a' => 'margin: 0 {{SIZE}}{{UNIT}};',
				// 		],
				// 	]
				// );
				$this->end_controls_section();
				$this->start_controls_section(
					'rig_nav_menu_dropdown_style',
					[
						'label' => __( 'Dropdown', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);
				$this->add_group_control(
					\Elementor\Group_Control_Typography::get_type(),
					[
						'name' => 'rig_nav_menu_dropdown_typography',
						'label' => __( 'Typography', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-nav-menu li ul li a',
					]
				);
				$this->start_controls_tabs(
					'rig_nav_menu_dropdown_tabs'
				);
				$this->start_controls_tab(
					'rig_nav_menu_dropdown_normal',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_text_color_normal',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_2,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul li a' => 'color: {{VALUE}}!important',
						],
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_background_color_normal',
					[
						'label' => __( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul' => 'background-color: {{VALUE}}!important',
						],
					]
				);
				$this->end_controls_tab();
				$this->start_controls_tab(
					'rig_nav_menu_dropdown_hover',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_text_color_hover',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul li a:hover' => 'color: {{VALUE}}!important',
						],
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_background_color_hover',
					[
						'label' => __( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul:hover' => 'background-color: {{VALUE}}!important',
						],
					]
				);
				$this->end_controls_tab();
				$this->start_controls_tab(
					'rig_nav_menu_dropdown_active',
					[
						'label' => __( 'Active', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_text_color_active',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul li a:active' => 'color: {{VALUE}}!important',
						],
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_background_color_active',
					[
						'label' => __( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'scheme' => [
							'type' => \Elementor\Scheme_Color::get_type(),
							'value' => \Elementor\Scheme_Color::COLOR_1,
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul:active' => 'background-color: {{VALUE}}',
						],
					]
				);
				$this->end_controls_tab();
				$this->end_controls_tabs();
				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'rig_nav_menu_dropdown_border',
						'label' => __( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-nav-menu li ul',
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_border_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%', 'em' ],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
					]
				);
				$this->add_group_control(
					\Elementor\Group_Control_Box_Shadow::get_type(),
					[
						'name' => 'rig_nav_menu_dropdown_box_shadow',
						'label' => __( 'Box Shadow', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-nav-menu li ul',
					]
				);
				$this->add_control(
					'rig_nav_menu_dropdown_divider',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);
				$this->add_responsive_control(
					'rig_nav_menu_dropdown_horizontal_padding',
					[
						'label' => __( 'Horizontal Padding', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul' => 'padding: 0 {{SIZE}}{{UNIT}};',
						],
					]
				);
				$this->add_responsive_control(
					'rig_nav_menu_dropdown_vertical_padding',
					[
						'label' => __( 'Vertical Padding', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul' => 'padding: {{SIZE}}{{UNIT}} 0;',
						],
					]
				);
				$this->add_responsive_control(
					'rig_nav_menu_dropdown_distance',
					[
						'label' => __( 'Distance', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}} .rig-nav-menu li ul' => 'margin-top: {{SIZE}}{{UNIT}};',
						],
					]
				);
				$this->end_controls_section();
				$this->start_controls_section(
					'rig_nav_menu_toggle_style',
					[
						'label' => __( 'Toggle', 'rig-elements' ),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
				);
				$this->start_controls_tabs(
					'rig_nav_menu_toggle_style_tabs'
				);
				$this->start_controls_tab(
					'rig_nav_menu_toggle_normal',
					[
						'label' => __( 'Normal', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_toggle_icon_normal_color',
					[
						'label' => __( 'Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button' => 'color: {{VALUE}}',
						],
					]
				);
				$this->add_control(
					'rig_nav_menu_toggle_background_normal_color',
					[
						'label' => __( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button' => 'background-color: {{VALUE}}',
						],
					]
				);
				$this->end_controls_tab();
				$this->start_controls_tab(
					'rig_nav_menu_toggle_hover',
					[
						'label' => __( 'Hover', 'rig-elements' ),
					]
				);
				$this->add_control(
					'rig_nav_menu_toggle_text_hover_color',
					[
						'label' => __( 'Text Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button:hover' => 'color: {{VALUE}}',
						],
					]
				);
				$this->add_control(
					'rig_nav_menu_toggle_background_hover_color',
					[
						'label' => __( 'Background Color', 'rig-elements' ),
						'type' => \Elementor\Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button:hover' => 'background-color: {{VALUE}}',
						],
					]
				);
				$this->end_controls_tab();
				$this->end_controls_tabs();
				$this->add_group_control(
					\Elementor\Group_Control_Border::get_type(),
					[
						'name' => 'rig_nav_menu_toggle_border',
						'label' => __( 'Border', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-mobile-menu-button',
					]
				);
				$this->add_group_control(
					\Elementor\Group_Control_Box_Shadow::get_type(),
					[
						'name' => 'rig_nav_menu_toggle_box_shadow',
						'label' => __( 'Box Shadow', 'rig-elements' ),
						'selector' => '{{WRAPPER}} .rig-mobile-menu-button',
					]
				);
				$this->add_control(
					'rig_nav_menu_toggle_divider',
					[
						'type' => \Elementor\Controls_Manager::DIVIDER,
					]
				);
				$this->add_responsive_control(
					'rig_nav_menu_toggle_size',
					[
						'label' => __( 'Size', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'range' => [
							'px' => [
								'min' => 10,
								'max' => 100,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button' => 'font-size: {{SIZE}}{{UNIT}};',
						],
					]
				);
				$this->add_responsive_control(
					'rig_nav_menu_toggle_border_radius',
					[
						'label' => __( 'Border Radius', 'rig-elements' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px','%' ],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 50,
								'step' => 1,
							],
							'%' => [
								'min' => 0,
								'max' => 100,
								'step' => 1,
							],
						],
						'selectors' => [
							'{{WRAPPER}} .rig-mobile-menu-button' => 'border-radius: {{SIZE}}{{UNIT}};',
						],
					]
				);
				$this->end_controls_section();
			}


			protected function render() {
				require_once( plugin_dir_path( __FILE__ ) . '/class-rig-navwalker.php' );
                $settings = $this->get_settings_for_display();
				$nav_align = $settings['rig_nav_menu_align'];
                $menu_id = $settings['rig_nav_menu_list'];
				$nav_menu_icon = $settings['rig_nav_menu_toggle_button'];

				$args = array(
					'menu' => $menu_id,	
					'container' => 'div',
					'container_class' => 'nav-menu-container',							
					'items_wrap'  => '<ul class="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">%3$s</ul>',
					'walker'          => new Rig_Menu_Walker(),
				);

				?>
				

		<nav class="px-2 bg-white border-gray-200 dark:bg-gray-800 dark:border-gray-700">
		
		<div class="nav-menu-container">
			
		<!-- Mobile Menu -->
		<button data-collapse-toggle="mobile-menu" type="button" class="inline-flex justify-center items-center ml-3 text-gray-400 rounded-lg md:hidden hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-blue-300 dark:text-gray-400 dark:hover:text-white dark:focus:ring-gray-500" aria-controls="mobile-menu-2" aria-expanded="false">
		<span class="sr-only">Open main menu</span>
		<svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
		<svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
		</button>

		<div class="hidden w-full md:block md:w-auto" id="mobile-menu">
		<ul class="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
		<li>
		<a href="#" class="block py-2 pr-4 pl-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-white dark:bg-blue-600 md:dark:bg-transparent" aria-current="page">Home</a>
		</li>
		<li>

		<!-- Dropdown Menu -->
		<button id="dropdownNavbarLink" data-dropdown-toggle="dropdownNavbar" class="flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">Dropdown <svg class="ml-1 w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg></button>

		<div id="dropdownNavbar" class="hidden z-10 w-44 text-base list-none bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600">

		<ul class="py-1" aria-labelledby="dropdownLargeButton">
		<li>
		<a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">Dashboard</a>
		</li>
		<li>
		<a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">Settings</a>
		</li>
		<li>
		<a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">Earnings</a>
		</li>
		</ul>
		<div class="py-1">
		<a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">Sign out</a>
		</div>
		</div>
		</li>

		<!-- Main Menu -->
		<li>
		<a href="#" class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400 dark:hover:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">Services</a>
		</li>

		<li>
		<a href="#" class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400 dark:hover:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">Pricing</a>
		</li>

		<li>
		<a href="#" class="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400 dark:hover:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">Contact</a>
		</li>

		</ul>

		</div>
		</div>
		</nav>

				
				<?php
				
				?>
				<script>
					jQuery(".rig-mobile-menu-button").click(function(){
					jQuery(".rig-nav-menu-responsive").toggle();
						});
					</script>
					<div class="toggle-btn-class"><button class="rig-mobile-menu-button"><i class="<?php echo $nav_menu_icon ?>"></i></button></div>
				<?php

				echo '<nav class="px-2 bg-white border-gray-200 dark:bg-gray-800 dark:border-gray-700">';
				echo wp_nav_menu( $args );
				echo '</nav>';

			}


			
		}
