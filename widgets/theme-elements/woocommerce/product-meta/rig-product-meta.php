<?php
		namespace RigElements\Widgets;

		use Elementor\Widget_Base;
		use Elementor\Controls_Manager;
        use Elementor\Group_Control_Border;
        use Elementor\Group_Control_Typography;
		use RigElements\Rig_Query_Control;

		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Product_Meta extends Widget_Base {


			public function get_name() {
				return 'rig-product-meta';
			}


			public function get_title() {
				return __( 'Product Meta', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-product-meta';
			}


			public function get_categories() {
				return [ 'rig_elements_woocommerce_widgets' ];
			}


			public function get_style_depends() {
				return [ 'core_css'];
			}

			public function get_script_depends() {
				return [ 'rig-main' ];
			}


			protected function register_controls() {
				$this->start_controls_section(
                    'rig_meta_content',
                    [
                        'label' => __( 'Content', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                    ]
                );
        
                $this->add_responsive_control(
                    'rig_meta_align',
                    [
                        'label' => __( 'Alignment', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::CHOOSE,
                        'options' => [
                            'left' => [
                                'title' => __( 'Left', 'rig-elements' ),
                                'icon' => 'eicon-text-align-left',
                            ],
                            'center' => [
                                'title' => __( 'Center', 'rig-elements' ),
                                'icon' => 'eicon-text-align-center',
                            ],
                            'right' => [
                                'title' => __( 'Right', 'rig-elements' ),
                                'icon' => 'eicon-text-align-right',
                            ],
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .product_meta' => 'text-align: {{VALUE}};',
                        ],
                        'default' => 'left',
                        'toggle' => true,
                    ]
                );
                $this->add_control(
                    'rig_meta_view',
                    [
                        'label' => __( 'View', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
                        'default' => 'solid',
                        'options' => [
                            'inline'  => __( 'Inline', 'rig-elements' ),
                            'block' => __( 'Stacked', 'rig-elements' ),
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .sku_wrapper' => 'display: {{VALUE}};',
                            '{{WRAPPER}} .posted_in' => 'display: {{VALUE}};',
                        ],
                    ]
                );
                $this->add_control(
                    'rig_meta_inline_space_between',
                    [
                        'label' => __( 'Inline Space Between', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => ['px'],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 50,
                                'step' => 5,
                            ],
                        ],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_meta_view',
                                    'operator' => '==',
                                    'value' => 'inline',
                                ],
                            ],
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .sku_wrapper' => 'padding-right: {{SIZE}}{{UNIT}};',
                            '{{WRAPPER}} .posted_in' => 'margin-left: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
                $this->add_control(
                    'rig_meta_stack_space_between',
                    [
                        'label' => __( 'Stack Space Between', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => ['px'],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 50,
                                'step' => 5,
                            ],
                        ],
                        'conditions' => [
                            'relation' => 'or',
                            'terms' => [
                                [
                                    'name' => 'rig_meta_view',
                                    'operator' => '==',
                                    'value' => 'block',
                                ],
                            ],
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .sku_wrapper' => 'padding-bottom: {{SIZE}}{{UNIT}};',
                            '{{WRAPPER}} .posted_in' => 'margin-top: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
                // $this->add_control(
                //     'rig_meta_content_hr_01',
                //     [
                //         'type' => \Elementor\Controls_Manager::DIVIDER,
                //     ]
                // );
                // $this->add_control(
                //     'rig_meta_divider_switch',
                //     [
                //         'label' => __( 'Divider', 'rig-elements' ),
                //         'type' => \Elementor\Controls_Manager::SWITCHER,
                //         'divider_on' => __( 'Show', 'your-plugin' ),
                //         'divider_off' => __( 'Hide', 'your-plugin' ),
                //         'return_value' => 'divider_on',
                //         'default' => 'divider_off',
                //     ]
                // );
                // $this->add_control(
                //     'rig_meta_divider_style',
                //     [
                //         'label' => __( 'Border Style', 'rig-elements' ),
                //         'type' => \Elementor\Controls_Manager::SELECT,
                //         'default' => 'solid',
                //         'options' => [
                //             'solid'  => __( 'Solid', 'rig-elements' ),
                //             'dashed' => __( 'Dashed', 'rig-elements' ),
                //             'dotted' => __( 'Dotted', 'rig-elements' ),
                //             'double' => __( 'Double', 'rig-elements' ),
                //             'none' => __( 'None', 'rig-elements' ),
                //         ],
                //         'conditions' => [
                //             'relation' => 'or',
                //             'terms' => [
                //                 [
                //                     'name' => 'rig_meta_divider_switch',
                //                     'operator' => '==',
                //                     'value' => 'divider_on',
                //                 ],
                //             ],
                //         ],
                //     ]
                // );
                $this->end_controls_section();
                $this->start_controls_section(
                    'rig_meta_style',
                    [
                        'label' => __( 'Style', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );
                $this->add_control(
                    'rig_meta_color',
                    [
                        'label' => __( 'Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .product_meta' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_meta_typography',
                        'label' => __( 'Typography', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .product_meta',
                    ]
                );
                $this->add_control(
                    'rig_meta_style_category_heading',
                    [
                        'label' => __( 'Cateogry', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'separator' => 'before',
                    ]
                );
                $this->add_control(
                    'rig_meta_category_color',
                    [
                        'label' => __( 'Color', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::COLOR,
                        // 'scheme' => [
                        //     'type' => \Elementor\Scheme_Color::get_type(),
                        //     'value' => \Elementor\Scheme_Color::COLOR_1,
                        // ],
                        'selectors' => [
                            '{{WRAPPER}} .posted_in a' => 'color: {{VALUE}}',
                        ],
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Typography::get_type(),
                    [
                        'name' => 'rig_meta_category_link_typography',
                        'label' => __( 'Typography', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .posted_in a',
                    ]
                );
                $this->end_controls_section();

			}


			protected function render() {
				$product_meta = (new Rig_Query_Control())->get_product_add_to_cart();
                $elementor_preview_mode = (new Rig_Query_Control())->is_elementor_preview_mode();

				if ($elementor_preview_mode == null) {
                    $product_meta = (new Rig_Query_Control())->get_product_add_to_cart();
                    echo '<div>'.woocommerce_template_single_meta().'</div>';
				}

				else {
					$product_preview_meta = (new Rig_Query_Control())->get_product_preview();
					echo '<div>'.woocommerce_template_single_meta().'</div>';
				}
				
			}
			
		}