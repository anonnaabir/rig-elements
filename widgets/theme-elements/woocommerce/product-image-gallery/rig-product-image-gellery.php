<?php
		namespace RigElements\Widgets;

		use Timber\Timber;
        use Elementor\Widget_Base;
		use Elementor\Controls_Manager;
		use RigElements\Rig_Query_Control;


		if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


		class Rig_Product_Image_Gallery extends Widget_Base {


			
			public function get_name() {
				return 'rig-product-image-gallery';
			}


			public function get_title() {
				return __( 'Product Image Gallery', 'rig-elements' );
			}


			public function get_icon() {
				return 'rig-product-image-gallery';
			}


			public function get_categories() {
				return [ 'rig_elements_woocommerce_widgets' ];
			}


			public function get_style_depends() {
				return [ 'rig-app'];
			}

			public function get_script_depends() {
				return [ 'rig-elements'];
			}


			protected function register_controls() {
				                               
                $this->start_controls_section(
                    'rig_product_image_gallery_style',
                    [
                        'label' => __( 'Style', 'rig-elements' ),
                        'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_responsive_control(
                    'rig_product_image_gallery_columns',
                    [
                        'label' => __( 'Columns', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
						'devices' => [ 'desktop', 'tablet', 'mobile' ],
						'desktop_default' => 'repeat(4, minmax(0, 1fr))',
						'tablet_default'  => 'repeat(3, minmax(0, 1fr))',
						'mobile_default'  => 'repeat(2, minmax(0, 1fr))',
                        'options' => [
                            'repeat(1, minmax(0, 1fr))' => __( '1 Columns', 'rig-elements' ),
							'repeat(2, minmax(0, 1fr))' => __( '2 Columns', 'rig-elements' ),
                            'repeat(3, minmax(0, 1fr))' => __( '3 Columns', 'rig-elements' ),
                            'repeat(4, minmax(0, 1fr))' => __( '4 Columns', 'rig-elements' ),
                            'repeat(5, minmax(0, 1fr))' => __( '5 Columns', 'rig-elements' ),
                            'repeat(6, minmax(0, 1fr))'  => __( '6 Columns', 'rig-elements' ),
                        ],

						'selectors' => [
							'{{WRAPPER}} .rig-product-gallery-container' => 'grid-template-columns: {{options}};',
						],
                    ]
                );

                $this->add_responsive_control(
                    'rig_product_image_gallery_width',
                    [
                        'label' => __( 'Image Width', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px', '%' ],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 500,
                                'step' => 1,
                            ],
                            '%' => [
                                'min' => 0,
                                'max' => 100,
                            ],
                        ],
                        'desktop_default' => [
                            'unit' => '%',
                            'size' => 100,
                        ],
						'tablet_default' => [
                            'unit' => '%',
                            'size' => 100,
                        ],
						'mobile_default' => [
                            'unit' => '%',
                            'size' => 100,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-product-gallery-container img' => 'width: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );


                $this->add_responsive_control(
                    'rig_product_image_gallery_height',
                    [
                        'label' => __( 'Image Height', 'rig-elements' ),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => [ 'px'],
                        'range' => [
                            'px' => [
                                'min' => 0,
                                'max' => 2500,
                                'step' => 1,
                            ],
                        ],
                        'desktop_default' => [
                            'unit' => 'px',
                            'size' => 300,
                        ],
						'tablet_default' => [
                            'unit' => 'px',
                            'size' => 300,
                        ],
						'mobile_default' => [
                            'unit' => 'px',
                            'size' => 150,
                        ],
                        'selectors' => [
                            '{{WRAPPER}} .rig-product-gallery-container img' => 'height: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );
                 
				$this->add_responsive_control(
                    'rig_product_image_gallery_fit',
                    [
                        'label' => __( 'Image Fit', 'rig-elements' ),
                        'type' => \Elementor\Controls_Manager::SELECT,
						'devices' => [ 'desktop', 'tablet', 'mobile' ],
						'desktop_default' => 'auto',
						'tablet_default'  => 'auto',
						'mobile_default'  => 'auto',
                        'options' => [
                            'contain'  => __( 'Contain', 'rig-elements' ),
                            'cover' => __( 'Cover', 'rig-elements' ),
                            'auto' => __( 'Auto', 'rig-elements' ),
                            'fill' => __( 'Fill', 'rig-elements' ),
                            'none' => __( 'None', 'rig-elements' ),
                        ],

						'selectors' => [
							'{{WRAPPER}} .rig-product-gallery-container img' => 'object-fit: {{options}}',
						],
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Border::get_type(),
                    [
                        'name' => 'rig_product_image_gallery_border',
                        'label' => __( 'Border', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-gallery-container img',
                    ]
                );
                $this->add_group_control(
                    \Elementor\Group_Control_Box_Shadow::get_type(),
                    [
                        'name' => 'rig_product_image_gallery_box_shadow',
                        'label' => __( 'Box Shadow', 'rig-elements' ),
                        'selector' => '{{WRAPPER}} .rig-product-gallery-container img',
                    ]
                );

                // $this->add_control(
                //     'rig_product_image_gallery_padding',
                //     [
                //         'label' => esc_html__( 'Margin', 'plugin-name' ),
                //         'type' => \Elementor\Controls_Manager::DIMENSIONS,
                //         'size_units' => [ 'px', '%', 'em' ],
                //         'selectors' => [
                //             '{{WRAPPER}} .rig-product-gallery-container img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                //         ],
                //     ]
                // );
                $this->end_controls_section();
			}


			protected function render() {
				$settings = $this->get_settings_for_display();
                $product_image_gallery = (new Rig_Query_Control())->get_product_image_gallery();
				$preview_image_gallery = (new Rig_Query_Control())->get_product_preview_gallery();
                $elementor_preview_mode = (new Rig_Query_Control())->is_elementor_preview_mode();
                
                echo '<div class="rig-product-gallery-container grid mobile:grid-cols-2 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 xl:grid-cols-4 gap-2" id="lightgallery">';
                
                if ($product_image_gallery != false && $elementor_preview_mode == false) {
                    foreach( $product_image_gallery as $image_gallery ) {
                        $image_link = wp_get_attachment_url( $image_gallery );
                        echo '<a href="'.$image_link.'"><img class="object-contain w-full h-96 p-2" src="'.$image_link.'" /></a>';
                    }
                }

                elseif ($product_image_gallery == false && $elementor_preview_mode == false) {
                    echo '';
                }

                elseif ($product_image_gallery == null && isset($elementor_preview_mode)) {
                    foreach( $preview_image_gallery as $image_gallery ) {
                        $image_link = wp_get_attachment_url( $image_gallery );
                        echo '<a href="'.$image_link.'"><img class="object-contain w-full h-96 p-2" src="'.$image_link.'" /></a>';
                    }
                }
                
                else {
                    echo '';
                }

                echo '</div>';
                
			}

		}






		