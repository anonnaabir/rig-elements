<?php
    namespace RigElements\Widgets;
    
    use Elementor\Widget_Base;
    use Elementor\Controls_Manager;

    if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class Rig_Soundcloud_Embed extends Widget_Base {
        
        public function get_name(){
            
            return 'rig-soundcloud';
        }

        public function get_title() {
            return __('SoundCloud', 'rig-elements');
        }

        public function get_icon() {
            return 'rig-soundcloud';
        }

        public function get_categories() {
            return ['rig_elements_widgets'];
        }

        public function get_style_depends() {
            return ['rig-app'];
        }

        public function get_script_depends() {
            return ['rig-elements'];
        }

        protected function _register_controls() {
            // Content Controls

            $this->start_controls_section(
                'rig_soundcloud_embed_contols',
            [
                'label' => __('Soundcloud Link', 'rig-elements'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]);

            $this->add_control(
                'soundcloud_link',
                [
                    'label' => esc_html__( 'Audio Link', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::URL,
                    'placeholder' => esc_html__( 'https://your-link.com', 'rig-elements' ),
                    'default' => [
                        'url' => 'https://soundcloud.com/bbc-world-service/a-world-without-oil',
                        'is_external' => true,
                        'nofollow' => true,
                        'custom_attributes' => '',
                    ],
                ]
            );
    

            $this->end_controls_section();


            // Style Controls

            $this->start_controls_section(
                'rig_soundcloud_embed_style',
            [
                'label' => __('Embed Background', 'rig-elements'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]);

            $this->add_control(
                'soundcloud_padding',
                [
                    'label' => esc_html__( 'Padding', 'rig-elements' ),
                    'type' => \Elementor\Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', '%', 'em' ],
                    'selectors' => [
                        '{{WRAPPER}} .rig-soundcloud-embed' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );

            $this->add_control(
                'soundcloud_background_color',
                [
                    'label' => esc_html__( 'Background Color', 'plugin-name' ),
                    'type' => \Elementor\Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .rig-soundcloud-embed' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
    
    


            $this->end_controls_section();

        }

        protected function render() {
            $settings = $this->get_settings_for_display();

            $url = $settings['soundcloud_link']['url'];
            // $args = array( 
            //     'width' => 612, 
            //     'height' => 344,
            //     'data-theme' => 'dark'
            // );

            $oembed = _wp_oembed_get_object();
            $oembed_provider = $oembed->get_provider( $url);
            $oembed_data = $oembed->fetch( $oembed_provider, $url);

            if ( $oembed_data ) {
                echo '<div class="rig-soundcloud-embed">'.$oembed_data->html.'</div>';
            }
            
        }
    }
