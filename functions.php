<?php

// Add specific CSS class by filter.

    // // if (is_404() && $template_type == '404') {
    //     add_filter('body_class', function($classes) {
    //         $classes[] = 'rig-404';
    //         return $classes;
    //     });
    // // }
 
// add_filter( 'body_class', function( $classes ) {
//   return array_merge( $classes, array( 'rig-404') );
// } );

// function my_plugin_body_class($classes) {
//   if (is_404()) {
//     $classes[] = 'rig-404';
//     return $classes;
//   }
  
// }

// add_filter('body_class', 'my_plugin_body_class');


function get_menu(){
  // get wordpress menus
  $menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
  // get menu items
  $menu_items = wp_get_nav_menu_items($menus[0]->term_id);
  // get menu items parent
  $menu_items_parent = wp_filter_object_list( $menu_items, array( 'menu_item_parent' => 0 ), 'and', 'ID' );
  // get menu items child
  $menu_items_child = wp_filter_object_list( $menu_items, array( 'menu_item_parent' => $menu_items_parent[0] ), 'and', 'ID' );
  
}

add_action('wp_footer', 'get_menu');

// check if elementor in edit mode
