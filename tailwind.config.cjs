/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    "../**.php",
    "../**/**.twig",
    "../**.twig",
    "../**/**.php",
    "./src/js/**.js",
    "./src/admin/js/**.{vue,js}",
  ],
  corePlugins: {
    preflight: true,
  },
  theme: {
    screens: {
      'mobile': '360px',
      ...defaultTheme.screens,
    },
    extend: {},
  },
  plugins: [],
}
