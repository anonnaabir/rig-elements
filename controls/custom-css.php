<?php
use \Elementor\Controls_Manager;


	if (!defined('ABSPATH')) {
		exit;
	} // Exit if accessed directly.

	class RigElements_Custom_CSS {

	/*
	 * Instance of this class
	 */
	private static $instance = null;

	public function __construct() {
		add_action( 'elementor/element/after_section_end', [ $this, 'rig_elements_custom_css_controls' ],25,3 );

		if (!defined('ELEMENTOR_PRO_VERSION')) {
			add_action('elementor/element/parse_css', [ $this, 'rig_elements_add_post_css' ],10,2 );
		}

		// add_action( 'elementor/css-file/post/parse', [ $this, 'rig_elements_add_page_settings_css' ] );
		// Add new controls to advanced tab globally
		// add_action("elementor/element/after_section_end", array($this, 'rig_elements_custom_css_controls'), 25, 3);

		// Render the custom CSS
		
	}



	public function rig_elements_custom_css_controls($widget, $section_id, $args) {
		if ('section_custom_css_pro' !== $section_id ) {
			return;
		}
		if (!defined('ELEMENTOR_PRO_VERSION')) {
			$widget->start_controls_section(
				'rig_elements_custom_css_section',
				array(
					'label'     => __('Custom CSS by Rig Elements', 'rig-elements'),
					'tab'       => Controls_Manager::TAB_ADVANCED
				)
			);

			$widget->add_control(
				'rig_elements_custom_css',
				array(
					'type'        => Controls_Manager::CODE,
					'label'       => __('Custom CSS', 'rig-elements'),
					'label_block' => true,
					'language'    => 'css'
				)
			);
			ob_start(); ?>
			<pre>
Examples:
// To target main element
selector { color: red; }
// For child element
selector .child-element{ margin: 10px; }
</pre><?php
			$output = ob_get_clean();

			$widget->add_control(
				'widgetkits_custom_css_description',
				array(
					'raw'             => __('Use "selector" keyword to target wrapper element.', 'rig-elements') . $output,
					'type'            => Controls_Manager::RAW_HTML,
					'content_classes' => 'elementor-descriptor',
					'separator'       => 'none'
				)
			);

			$widget->end_controls_section();
		}
	}



	public function rig_elements_add_post_css($post_css, $element) {
		$element_settings = $element->get_settings();

		if (empty($element_settings['rig_elements_custom_css'])) {
			return;
		}

		$css = trim($element_settings['rig_elements_custom_css']);

		if (empty($css)) {
			return;
		}
		$css = str_replace('selector', $post_css->get_element_unique_selector($element), $css);

		// Add a css comment
		$css = sprintf('/* Start custom CSS for %s, class: %s */', $element->get_name(), $element->get_unique_selector()) . $css . '/* End custom CSS */';

		$post_css->get_stylesheet()->add_raw_css($css);
	}

	public function rig_elements_add_page_settings_css( $post_css ) {
		$document = \Elementor\Plugin::$instance->documents->get( $post_css->get_post_id() );
		$custom_css = $document->get_settings( 'rig_elements_custom_css' );
		$custom_css = trim( $custom_css );

		if ( empty( $custom_css ) ) {
			return;
		}

		$custom_css = str_replace( 'selector', $document->get_css_wrapper_selector(), $custom_css );

		// Add a css comment
		$custom_css = '/* Start custom CSS for page-settings */' . $custom_css . '/* End custom CSS */';

		$post_css->get_stylesheet()->add_raw_css( $custom_css );
	}



	public static function get_instance() {
		if (!self::$instance) {
			self::$instance = new self;
		}
		return self::$instance;
	}
}

RigElements_Custom_CSS::get_instance();

